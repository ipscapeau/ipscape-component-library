import { create } from '@storybook/theming/create';
import logoImg from './logo.svg';

export default create({
  base: 'light',
  brandTitle: 'ipSCAPE component library',
  brandUrl: 'https://ipscape.com',
  brandImage: logoImg,
});
