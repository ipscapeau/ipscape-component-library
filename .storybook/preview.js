import { addParameters, addDecorator } from '@storybook/vue';
import { setConsoleOptions } from '@storybook/addon-console';
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';
import ipscapeTheme from './ipscapeTheme';

addParameters({
  options: {},
  viewport: {
    viewports: INITIAL_VIEWPORTS,
  },
  a11y: {
    element: '#root',
    config: {},
    options: {},
    manual: false,
  },
  docs: {
    theme: ipscapeTheme,
  },
});

addDecorator(() => ({
  template: "<div class='container-fluid'><div><story/></div></div>",
}));

setConsoleOptions({
  panelExclude: [],
});
