module.exports = {
  stories: ['../**/*.stories.@(js|mdx|ts)'],
  addons: [
    '@storybook/addon-a11y/',
    '@storybook/addon-storysource',
    '@storybook/addon-actions/',
    {
      name: '@storybook/addon-docs',
      options: {
        sourceLoaderOptions: {
          injectStoryParameters: false,
        },
      },
    },
    '@storybook/addon-controls',
    '@storybook/addon-links',
    '@storybook/addon-viewport',
    '@storybook/addon-postcss',
  ],
};
