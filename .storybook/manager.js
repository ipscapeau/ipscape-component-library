import { addons } from '@storybook/addons';
import ipscapeTheme from './ipscapeTheme';

addons.setConfig({
  theme: ipscapeTheme,
});
