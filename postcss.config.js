const purgecss = require('@fullhuman/postcss-purgecss');

module.exports = {
  plugins: [
    // eslint-disable-next-line no-undef
    purgecss({
      css: ['./packages/styles/custom.scss'],
      content: ['./src/**/Ips*.vue', './src/**/index.js'],
    }),
  ],
};
