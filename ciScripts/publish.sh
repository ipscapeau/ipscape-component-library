#!/bin/bash

# variables passed to script
DIST_TAG=$1

echo "Publish npm packages";
printf "//`node -p \"require('url').parse(process.env.NPM_REGISTRY_URL || 'https://registry.npmjs.org').host\"`/:_authToken=${NPM_REGISTRY_TOKEN}\nregistry=${NPM_REGISTRY_URL:-https://registry.npmjs.org}\n" >> ~/.npmrc
lerna publish from-package --no-git-reset --dist-tag "$DIST_TAG" --yes
