export default {
  name: 'alertMixin',
  data() {
    return {};
  },
  props: {
    variant: {
      name: 'variant',
      description: 'The variant of the alert',
      type: String,
      default: 'message',
      required: false,
      validator: (value) =>
        ['message', 'danger', 'info', 'success', 'warning'].indexOf(value) !== -1,
    },
    title: {
      name: 'title',
      description: 'The alert title',
      type: String,
      default: null,
      required: false,
    },
    message: {
      name: 'message',
      description: 'The alert message',
      type: String,
      default: null,
      required: false,
    },
  },
  computed: {
    iconClasses() {
      const arr = [];
      if (this.variant) arr.push(`text-${this.variant}`);
      if (this.variant === 'message') arr.push('text-secondary');
      return arr;
    },
    iconType() {
      switch (this.variant) {
        case 'danger':
          return 'ico-danger';
        case 'info':
          return 'ico-info';
        case 'success':
          return 'ico-success';
        case 'warning':
          return 'ico-warning';
        default:
          return '';
      }
    },
    role() {
      return this.variant === 'danger' ? 'alert' : 'status';
    },
  },
};
