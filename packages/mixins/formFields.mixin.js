export default {
  name: 'formFieldsMixin',
  data() {
    return {
      isOpen: false,
      isDirty: false,
      preferredOpenDirection: 'below',
      optimizedHeight: this.maxHeight,
    };
  },
  props: {
    /**
     * Sets the id of the component
     * @default null
     */
    id: {
      name: 'id',
      description: 'The id of the input',
      type: String,
      default: null,
      required: true,
    },
    /**
     * Presets the value
     */
    value: {
      name: 'value',
      description: 'The value',
    },
    /**
     * Array of classes to be applied to the form element
     * @default []
     */
    classes: {
      name: 'classes',
      description: 'The classes',
      type: Array,
      default() {
        return [];
      },
      required: false,
    },
    /**
     * Array of classes to be applied to the label
     * @default []
     */
    labelClasses: {
      name: 'labelClasses',
      description: 'The label classes',
      type: Array,
      default() {
        return [];
      },
      required: false,
    },
    /**
     * Sizing control for elements
     * @default medium
     */
    sizing: {
      name: 'sizing',
      description: 'The sizing',
      type: String,
      default: 'medium',
      required: false,
      validator: (value) => ['small', 'medium', 'large'].indexOf(value) !== -1,
    },
    /**
     * Set the max height for the options display
     * @default null
     */
    maxHeight: {
      name: 'maxHeight',
      description: 'The max height',
      type: String,
      default: null,
      required: false,
    },
    /**
     * Sets the label of the component
     * @default null
     */
    label: {
      name: 'label',
      description: 'The label',
      type: String,
      default: null,
      required: true,
    },
    /**
     * Shows the label if true
     * @default false
     */
    displayLabel: {
      name: 'displayLabel',
      description: 'Display the label',
      type: Boolean,
      default: false,
      required: false,
    },
    /**
     * Clear the input on focus
     * @default true
     */
    clearOnFocus: {
      name: 'clearOnFocus',
      description: 'The clear on focus',
      type: Boolean,
      default: true,
      required: false,
    },
    /**
     * Equivalent to the `placeholder` attribute on input
     * @default null
     */
    placeholder: {
      name: 'placeholder',
      description: 'The placeholder',
      type: String,
      default: null,
      required: false,
    },
    /**
     * Disable the input
     * @default false
     */
    disabled: {
      name: 'disabled',
      description: 'The disabled',
      type: Boolean,
      default: false,
      required: false,
    },
    /**
     * Make a required field
     * @default false
     */
    required: {
      name: 'required',
      description: 'The required',
      type: Boolean,
      default: false,
      required: false,
    },
    /**
     * Triggers field error state
     * @default false
     */
    error: {
      name: 'error',
      description: 'The error',
      type: Boolean,
      default: false,
      required: false,
    },
    /**
     * Sets the text of the tooltip
     * @default null
     */
    tooltip: {
      name: 'tooltip',
      description: 'The tooltip',
      type: String,
      default: '',
      required: false,
    },
    /**
     * Adds icon as prepend to input field
     * @default null
     */
    icon: {
      name: 'icon',
      description: 'The icon',
      type: String,
      default: null,
      required: false,
    },
    /**
     * Adds icon as append to input field
     * @default ''
     */
    iconAppend: {
      name: 'iconAppend',
      description: 'The icon append',
      type: String,
      default: '',
      required: false,
    },
    /**
     * Read only plain text
     * @default false
     */
    plainText: {
      name: 'plainText',
      description: 'The plain text',
      type: Boolean,
      default: false,
      required: false,
    },
    /**
     * Makes input read only
     * @default false
     */
    readonly: {
      name: 'readonly',
      description: 'Makes input read only',
      type: Boolean,
      default: false,
      required: false,
    },
    /**
     * Shows icon to clear input
     * @default false
     */
    clearable: {
      name: 'clearable',
      description: 'Adds clear icon to input field.',
      type: Boolean,
      default: false,
      required: false,
    },
    /**
     * Tab index of the element
     * @default 0
     */
    tabindex: {
      name: 'tabindex',
      description: 'Adds tab index to element',
      type: Number,
      default: null,
      required: false,
    },
    /**
     * autocomplete element
     * @default on
     */
    autocomplete: {
      name: 'autocomplete',
      description: 'Autocomplete flag for the input field',
      type: String,
      default: 'on',
      required: false,
    },
    /**
     * Set the max height for the options display
     * @default null
     */
    maxlength: {
      name: 'maxlength',
      description: 'The max length',
      type: Number,
      default: null,
      required: false,
    },
  },
  computed: {
    labelComputedClasses() {
      const computedClasses = ['ips', 'default-label', 'form-label'];
      if (this.labelClasses.length > 0) {
        this.labelClasses.forEach((item) => {
          computedClasses.push(item);
        });
      }
      if (!this.displayLabel) computedClasses.push('visually-hidden');
      if (this.error) computedClasses.push('has-validation');
      if (this.required && this.inputType !== 'radio') computedClasses.push('is-required');
      return computedClasses;
    },
    formComputedClasses() {
      return ['ips', 'default-input'];
    },
    controlSizing() {
      switch (this.sizing) {
        case 'small':
          return 'form-control-sm';
        case 'large':
          return 'form-control-lg';
        case 'medium':
        default:
          return '';
      }
    },
    inputComputedClasses() {
      const computedClasses = ['ips', this.controlSizing];
      if (this.classes.length > 0) {
        this.classes.forEach((item) => {
          computedClasses.push(item);
        });
      }
      if (this.error) computedClasses.push('is-invalid');
      if (this.plainText) computedClasses.push('form-control-plaintext');
      if (this.type === 'search') computedClasses.push('has-icon--prepend');
      if (this.icon) computedClasses.push('has-icon--prepend');
      if (this.iconAppend) computedClasses.push('has-icon--append');
      return computedClasses;
    },
  },
  methods: {
    appendIconClicked(event) {
      this.$emit('append-icon-clicked', event);
    },
  },
  watch: {
    value: {
      immediate: true,
      handler(newVal) {
        this.$nextTick(() => {
          this.isDirty = newVal ? newVal.length > 0 : false;
        });
      },
    },
  },
};
