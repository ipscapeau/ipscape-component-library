// Static

export const { from } = Array;
export const { isArray } = Array;

// Instance

export const arrayIncludes = (array, value) => array.indexOf(value) !== -1;
export const concat = (...args) => Array.prototype.concat.apply([], args);
