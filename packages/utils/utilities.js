export function shortenText(text, limit) {
  return text && text.length >= limit ? text.substring(0, limit).concat('...') : text;
}
