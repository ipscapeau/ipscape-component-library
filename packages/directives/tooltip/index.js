import Vue from 'vue';
import { Tooltip } from 'bootstrap';

export default Vue.directive('tooltip', {
  bind: function (el, binding) {
    // default bootstrap config options
    const config = {
      animation: true,
      container: 'body',
      delay: { show: 500, hide: 100 },
      html: false,
      placement: 'top',
      selector: false,
      title: '',
      trigger: 'hover',
      fallbackPlacements: ['top', 'right', 'bottom', 'left'],
      customClass: '',
      sanitize: true,
      sanitizeFn: null,
      offset: [0, 0],
      popperConfig: null,
    };

    // override default config
    if ('animation' in binding.value) {
      config.animation = binding.value.animation;
    }
    if ('container' in binding.value) {
      config.container = binding.value.container;
    }
    if ('delay' in binding.value) {
      config.delay = binding.value.delay;
    }
    if ('html' in binding.value) {
      config.html = binding.value.html;
    }
    if ('placement' in binding.value) {
      config.placement = binding.value.placement;
    }
    if ('title' in binding.value) {
      config.title = binding.value.title;
    }
    if ('trigger' in binding.value) {
      config.trigger = binding.value.trigger;
    }
    if ('sanitize' in binding.value) {
      config.sanitize = binding.value.sanitize;
    }
    if ('offset' in binding.value) {
      config.offset = binding.value.offset;
    }

    new Tooltip(el, config);
  },
  inserted: function (el, binding) {
    let onlyWhenTruncated = false;
    if ('onlyWhenTruncated' in binding.value) {
      onlyWhenTruncated = binding.value.onlyWhenTruncated;
    }

    if (onlyWhenTruncated && el.scrollWidth <= el.clientWidth) {
      if (el.scrollHeight <= el.clientHeight) {
        Tooltip.getInstance(el).dispose();
      }
    }
  },
  unbind: function (el) {
    if (Tooltip.getInstance(el)) {
      Tooltip.getInstance(el).dispose();
    }
  },
});
