import IpsTelInput from '../src/IpsTelInput.vue';

export default {
  title: 'Components/IpsTelInput',
  component: IpsTelInput,
  argTypes: {
    sizing: {
      control: {
        type: 'select',
        options: ['small', 'large'],
      },
    },
  },
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsTelInput },
  template: `<ips-tel-input
    :id="id"
    :value="value"
    :classes="classes"
    :label-classes="labelClasses"
    :sizing="sizing"
    :max-height="maxHeight"
    :label="label"
    :display-label="displayLabel"
    :clear-on-focus="clearOnFocus"
    :placeholder="placeholder"
    :required="required"
    :error="error"
    :tooltip="tooltip"
    :icon="icon"
    :iconAppend="iconAppend"
    :clearable="clearable"
    :disabled="disabled"
    :input-id="inputId"
    :show-arrow-icon="showArrowIcon"
    :show-dial-code-in-list="showDialCodeInList"
    :show-dial-code-in-selection="showDialCodeInSelection"
    :show-flags="showFlags"
    :maxlength="maxlength"
    :autofocus="autofocus"
    :name="name"
    :input-style-classes="inputStyleClasses"
    :auto-format="autoFormat"
    :custom-validate="customValidate"
    :default-country="defaultCountry"
    :auto-default-country="autoDefaultCountry"
    :ignored-countries="ignoredCountries"
    :invalid-msg="invalidMsg"
    :mode="mode"
    :only-countries="onlyCountries"
    :preferred-countries="preferredCountries"
    :style-classes="styleClasses"
    :valid-characters-only="validCharactersOnly"
    :validation-type="validationType"
    :allow-emergency-number="allowEmergencyNumber"
></ips-tel-input>`,
});

export const basic = template.bind({});
basic.args = {
  id: 'basic',
  label: 'Basic label',
};

export const validationTypePossible = template.bind({});
validationTypePossible.args = {
  id: 'validationTypePossible',
  label: 'The label',
  validationType: 'possible',
};

export const allowEmergencyNumber = template.bind({});
allowEmergencyNumber.args = {
  id: 'allowEmergnecyNumber',
  label: 'The label',
  allowEmergencyNumber: true,
};
