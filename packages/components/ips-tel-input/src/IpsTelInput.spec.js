import { mount } from '@vue/test-utils';
import IpsTelInput from './IpsTelInput.vue';

describe('IpsTelInput.vue', () => {
  let wrapper;

  beforeEach(function () {
    wrapper = mount(IpsTelInput, {
      data: () => {
        return {
          isDirty: true,
        };
      },
      propsData: { id: 'id', label: 'label' },
    });
  });

  test.skip('Renders default tel input field', () => {
    expect(wrapper.html()).toContain('form-control vue-tel-input');
  });

  test.skip('should render a tooltip', async () => {
    await wrapper.setProps({ tooltip: 'Test tooltip' });
    const tip = wrapper.find('span[data-bs-toggle="tooltip"]');
    expect(tip).toBeDefined();
  });

  test.skip('Clears input when clicking on clear icon', () => {
    expect(wrapper.html()).toContain('form-control vue-tel-input');
  });

  test.skip('Input field is clearable', async () => {
    await wrapper.setProps({ clearable: true, value: 'q' });
    expect(wrapper.html()).toContain('clearable="true"');
  });

  test.skip('Emits input event', async () => {
    await wrapper.find('input').trigger('input');
  });

  test.skip('Emits clear input click event', async () => {
    await wrapper.setProps({ clearable: true });
    await wrapper.setData({ phoneNumber: '1234' });
    expect(wrapper.find('#id input').element.value).toEqual('1234');
    await wrapper.find('i.ico-delete').trigger('click');
    expect(wrapper.find('#id input').element.value).toEqual('');
  });

  test.skip('Does not clear input when disabled', async () => {
    await wrapper.setProps({ clearable: true, disabled: true });
    await wrapper.setData({ phoneNumber: '1234' });
    expect(wrapper.find('#id input').element.value).toEqual('1234');
    await wrapper.find('i.ico-delete').trigger('click');
    expect(wrapper.find('#id input').element.value).toEqual('1234');
  });

  test.skip('should include "is-required" if "required" is set', async () => {
    await wrapper.setProps({ required: true });
    const expected = ['is-required'];
    expect(wrapper.vm.labelComputedClasses).toEqual(expect.arrayContaining(expected));
  });
});
