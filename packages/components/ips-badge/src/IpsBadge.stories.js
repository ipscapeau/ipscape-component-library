import IpsBadge from '../src/IpsBadge.vue';

export default {
  title: 'Components/IpsBadge',
  component: IpsBadge,
  argTypes: {
    label: {
      control: {
        type: 'text',
      },
      defaultValue: 'Badge label',
    },
    variant: {
      control: {
        type: 'select',
        options: ['blue', 'green', 'gray', 'orange', 'purple', 'red', 'teal', 'yellow'],
      },
    },
    variantStyle: {
      control: {
        type: 'select',
        options: ['solid', 'subtle'],
      },
    },
  },
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsBadge },
  template: '<ips-badge :variant="variant" :variant-style="variantStyle" >{{ label }}</ips-badge>',
});

export const basic = template.bind({});
basic.args = {
  variant: 'gray',
  variantStyle: 'solid',
};
