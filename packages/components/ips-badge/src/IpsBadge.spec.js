import { shallowMount } from '@vue/test-utils';
import IpsBadge from './IpsBadge.vue';

let wrapper;

describe('IpsBadge.vue', () => {
  beforeEach(function () {
    wrapper = shallowMount(IpsBadge);
  });

  test('Renders default badge', () => {
    expect(wrapper.html()).toContain('class="badge badge-gray-solid');
  });

  test('Renders solid badge', async () => {
    await wrapper.setProps({ variantStyle: 'solid' });
    expect(wrapper.html()).toContain('class="badge badge-gray-solid');
  });

  test('Renders subtle badge', async () => {
    await wrapper.setProps({ variantStyle: 'subtle' });
    expect(wrapper.html()).toContain('class="badge badge-gray-subtle');
  });

  test('Renders blue badge', async () => {
    await wrapper.setProps({ variant: 'blue' });
    expect(wrapper.html()).toContain('class="badge badge-blue-solid');
  });

  test('Renders subtle blue badge', async () => {
    await wrapper.setProps({ variant: 'blue', variantStyle: 'subtle' });
    expect(wrapper.html()).toContain('class="badge badge-blue-subtle');
  });

  test('Renders green badge', async () => {
    await wrapper.setProps({ variant: 'green' });
    expect(wrapper.html()).toContain('class="badge badge-green-solid');
  });

  test('Renders subtle green badge', async () => {
    await wrapper.setProps({ variant: 'green', variantStyle: 'subtle' });
    expect(wrapper.html()).toContain('class="badge badge-green-subtle');
  });

  test('Renders neutral badge', async () => {
    await wrapper.setProps({ variant: 'gray' });
    expect(wrapper.html()).toContain('class="badge badge-gray-solid');
  });

  test('Renders subtle neutral badge', async () => {
    await wrapper.setProps({ variant: 'gray', variantStyle: 'subtle' });
    expect(wrapper.html()).toContain('class="badge badge-gray-subtle');
  });

  test('Renders orange badge', async () => {
    await wrapper.setProps({ variant: 'orange' });
    expect(wrapper.html()).toContain('class="badge badge-orange-solid');
  });

  test('Renders subtle orange badge', async () => {
    await wrapper.setProps({ variant: 'orange', variantStyle: 'subtle' });
    expect(wrapper.html()).toContain('class="badge badge-orange-subtle');
  });

  test('Renders purple badge', async () => {
    await wrapper.setProps({ variant: 'purple' });
    expect(wrapper.html()).toContain('class="badge badge-purple-solid');
  });

  test('Renders subtle purple badge', async () => {
    await wrapper.setProps({ variant: 'purple', variantStyle: 'subtle' });
    expect(wrapper.html()).toContain('class="badge badge-purple-subtle');
  });

  test('Renders red badge', async () => {
    await wrapper.setProps({ variant: 'red' });
    expect(wrapper.html()).toContain('class="badge badge-red-solid');
  });

  test('Renders subtle red badge', async () => {
    await wrapper.setProps({ variant: 'red', variantStyle: 'subtle' });
    expect(wrapper.html()).toContain('class="badge badge-red-subtle');
  });

  test('Renders teal badge', async () => {
    await wrapper.setProps({ variant: 'teal' });
    expect(wrapper.html()).toContain('class="badge badge-teal-solid');
  });

  test('Renders subtle teal badge', async () => {
    await wrapper.setProps({ variant: 'teal', variantStyle: 'subtle' });
    expect(wrapper.html()).toContain('class="badge badge-teal-subtle');
  });

  test('Renders yellow badge', async () => {
    await wrapper.setProps({ variant: 'yellow' });
    expect(wrapper.html()).toContain('class="badge badge-yellow-solid');
  });

  test('Renders subtle yellow badge', async () => {
    await wrapper.setProps({ variant: 'yellow', variantStyle: 'subtle' });
    expect(wrapper.html()).toContain('class="badge badge-yellow-subtle');
  });
});
