import IpsTooltip from './IpsTooltip';
import IpsButton from '../../ips-button/src/IpsButton.vue';
import IpsBadge from '../../ips-badge/src/IpsBadge.vue';

export default {
  title: 'Components/IpsTooltip',
  component: IpsTooltip,
  argTypes: {
    tooltip: {
      control: {
        type: 'text',
      },
      defaultValue: 'This is the display tip',
    },
    placement: {
      control: {
        type: 'select',
        label: 'Tip Placement',
        options: ['auto', 'top', 'right', 'bottom', 'left'],
      },
      defaultValue: 'bottom',
    },
  },
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsTooltip },
  template:
    '<p>This is a <ips-tooltip :tooltip="tooltip" :placement="placement" :entry-delay="entryDelay"' +
    ' :exit-delay="exitDelay"><template slot="tip"><a href="#">sample</a></template></ips-tooltip>' +
    ' with a tooltip.</p>',
});

const btnTemplate = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsTooltip, IpsButton },
  template:
    '<ips-tooltip :tooltip="tooltip" :trigger="trigger" :placement="placement" :entry-delay="entryDelay"' +
    ' :exit-delay="exitDelay"><template v-slot:tip="scope"><ips-button @click="clickHandler({ scope})"' +
    ' :variant="variant" size="md" :type="type">{{ label }}</ips-button>' +
    '</template></ips-tooltip>',
  methods: {
    clickHandler({ scope }) {
      if (scope && typeof scope.hide === 'function') scope.hide();
    },
  },
});

const htmlTemplate = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsTooltip },
  template:
    '<p>This tooltip <ips-tooltip :tooltip="tooltip" :html="true" :placement="placement"' +
    ' :entry-delay="entryDelay"' +
    ' :exit-delay="exitDelay"><template slot="tip"><a href="#">example</a></template></ips-tooltip> renders HTML' +
    ' within the tip.</p>',
});

const badgeTemplate = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsTooltip, IpsBadge },
  template:
    '<ips-tooltip :tooltip="tooltip" :placement="placement" :entry-delay="entryDelay" :exit-delay="exitDelay">' +
    '<template slot="tip"><ips-badge :variant="variant" :variant-style="variantStyle">{{ label' +
    ' }}</ips-badge></template>' +
    '</ips-tooltip>',
});

export const simple = template.bind({});
simple.args = {
  entryDelay: 1000,
  exitDelay: 800,
};

export const button = btnTemplate.bind({});
button.args = {
  entryDelay: 1000,
  exitDelay: 800,
  trigger: 'hover',
  variant: 'success',
  type: 'button',
  label: 'Submit',
};

export const HTML_Tip = htmlTemplate.bind({});
HTML_Tip.args = {
  entryDelay: 1000,
  exitDelay: 1800,
  tooltip:
    'Click <a href="//getbootstrap.com/docs/5.0/components/tooltips" target="_blank">here</a> for help' +
    ' on tooltips',
};

export const Badge = badgeTemplate.bind({});
Badge.args = {
  entryDelay: 100,
  exitDelay: 100,
  placement: 'right',
  variant: 'orange',
  variantStyle: 'solid',
  tooltip: 'Calls in queue',
  label: 5,
};
