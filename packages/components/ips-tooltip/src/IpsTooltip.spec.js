import { shallowMount } from '@vue/test-utils';
import IpsTooltip from './IpsTooltip';

let wrapper;

describe('IpsTooltip', () => {
  beforeEach(() => {
    wrapper = shallowMount(IpsTooltip, {
      propsData: {
        tooltip: 'This is a sample tooltip',
      },
    });
  });

  test('is a Vue instance', () => {
    expect(wrapper.isVueInstance).toBeTruthy();
  });

  test('renders default markup', () => {
    expect(wrapper.vm.$refs.ipsTip).toBeDefined();
  });
});
