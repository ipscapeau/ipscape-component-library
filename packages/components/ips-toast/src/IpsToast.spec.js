import { mount } from '@vue/test-utils';
import IpsToast from './IpsToast.vue';

let wrapper;

describe('IpsToast.vue', () => {
  beforeEach(() => {
    wrapper = mount(IpsToast, {
      propsData: {
        title: 'Test title',
        message: 'Test toast',
        dismissible: true,
        timeout: 3000,
      },
    });
  });

  test('toast heading contains title', () => {
    expect(wrapper.find('.toast-heading').exists()).toBe(true);
    const body = wrapper.find('.toast-heading');
    expect(body.html()).toContain('Test title');
  });

  test('toast body contains message', async () => {
    expect(wrapper.find('.toast-body').exists()).toBe(true);
    const body = wrapper.find('.toast-body');
    expect(body.html()).toContain('Test toast');
  });

  test('Emits dismiss click event', async () => {
    await wrapper.setProps({ dismissible: true });
    await wrapper.find('button').trigger('click');
  });

  test('Emits toast-button-clicked click event', async () => {
    await wrapper.setProps({ buttonText: 'Button' });
    const body = wrapper.find('.toast-body');
    await body.find('button').trigger('click');
    expect(wrapper.emitted('toast-button-clicked')).toBeTruthy();
  });

  describe('Computed props', () => {
    describe('isDismissible()', () => {
      beforeEach(async () => {
        await wrapper.setProps({ dismissible: true });
      });

      test('should return a boolean', () => {
        expect(typeof wrapper.vm.isDismissible).toBe('boolean');
      });
    });

    describe('classControl()', () => {
      test('should return an array', () => {
        expect(typeof wrapper.vm.classControl).toBe('object');
      });

      test('should return "toast-danger" if prop set', async () => {
        await wrapper.setProps({ variant: 'danger' });
        const expected = ['toast-danger'];
        expect(wrapper.vm.classControl).toEqual(expect.arrayContaining(expected));
      });

      test('should return "toast-info" if classes prop is set', async () => {
        await wrapper.setProps({ variant: 'info' });
        const expected = ['toast-info'];
        expect(wrapper.vm.classControl).toEqual(expect.arrayContaining(expected));
      });

      test('should return "toast-success" if classes prop is set', async () => {
        await wrapper.setProps({ variant: 'success' });
        const expected = ['toast-success'];
        expect(wrapper.vm.classControl).toEqual(expect.arrayContaining(expected));
      });

      test('should return "toast-warning" if classes prop is set', async () => {
        await wrapper.setProps({ variant: 'warning' });
        const expected = ['toast-warning'];
        expect(wrapper.vm.classControl).toEqual(expect.arrayContaining(expected));
      });
    });
  });
});
