import IpsToast from './IpsToast.vue';
import IpsTheme from '../../ips-theme/src/IpsTheme';
import IpsButton from '../../ips-button/src/IpsButton.vue';
import IpsIcon from '../../ips-icon/src/IpsIcon';

export default {
  title: 'Components/IpsToast',
  component: IpsToast,
  argTypes: {
    variant: {
      control: {
        type: 'select',
        options: ['message', 'danger', 'info', 'success', 'warning'],
      },
    },
  },
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsToast },
  template:
    '<div><ips-toast :dismissible="dismissible" :timeout="timeout" :button-text="buttonText" :variant="variant" :title="title" :message="message"></ips-toast></div>',
});

const complexTemplate = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsTheme, IpsToast, IpsButton, IpsIcon },
  template: `<ips-theme>
      <ips-toast
        variant="info"
        :timeout="timeout"
        :dismissible="dismissible"
      >
        <template v-slot:content="content">
          <div class="d-flex w-100">
            <div>
              <div class="col ms-1 px-1"><b>Inbound Call</b></div>
            </div>
            <div class="ms-auto">
              <ips-button variant="danger" icon="ico-hangup" size="sm" @click.prevent="clickMe({ scope: content })" aria-label="Dismiss Call">
              </ips-button>
              <ips-button variant="success" icon="ico-phone" size="sm" @click.prevent="clickMe({ scope: content })" aria-label="Answer Call">
              </ips-button>
            </div>
          </div>
        </template>
      </ips-toast>
    </ips-theme>`,
  methods: {
    clickMe({ scope }) {
      console.log('Clicked', scope);
      if (typeof scope.close === 'function') scope.close();
    },
  },
});

export const basic = template.bind({});
basic.args = {
  title: 'Toast title',
  message: 'Toast message',
  timeout: 0,
};

export const button = template.bind({});
button.args = {
  title: 'Toast title',
  message: 'Toast message',
  buttonText: 'Custom Button',
  timeout: 0,
};

export const complex = complexTemplate.bind({});
complex.args = {
  dismissible: false,
  timeout: 0,
};

export const timeout = template.bind({});
timeout.args = {
  title: 'Toast with Timeout',
  variant: 'info',
  message: 'This message will close after 5 seconds',
  timeout: 5000,
};
