import IpsDropdown from '../src/IpsDropdown.vue';
import IpsAvatar from '../../ips-avatar/src/IpsAvatar.vue';
import IpsButton from '../../ips-button/src/IpsButton.vue';
import IpsIcon from '../../ips-icon/src/IpsIcon.vue';

export default {
  title: 'Components/IpsDropdown',
  component: IpsDropdown,
  argTypes: {},
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsDropdown, IpsButton },
  template: `<ips-dropdown :disabled="disabled" :disable-outside-click="disableOutsideClick" :expanded="expanded" :aria-described-by="ariaDescribedBy" :classes="classes" :drop-right="dropRight">
     <template slot="trigger">
       <ips-button id="dropdownMenuButton" class="dropdown-toggle">Dropdown button</ips-button>
     </template>
     <template slot="items">
       <a class="dropdown-item" href="#">Item 1</a>
       <a class="dropdown-item" href="#">Item 2</a>
       <a class="dropdown-item" href="#">Item 3</a>
     </template>
    </ips-dropdown>`,
});

const templateSelect = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsDropdown, IpsButton, IpsIcon },
  template: `<ips-dropdown :disabled="disabled" :expanded="expanded" :aria-described-by="ariaDescribedBy" :classes="classes" :drop-right="dropRight">
        <template slot="trigger">
          <ips-button id="dropdownMenuButton" class="dropdown-toggle">Dropdown button</ips-button>
        </template>
        <template slot="items">
          <a class="dropdown-item" href>
            <div class="dropdown-item-left">
              <ips-icon icon="ico-tick"></ips-icon>
            </div>
            <div class="dropdown-item-label">Dropdown label 1</div>
          </a>
          <a class="dropdown-item" href>
            <div class="dropdown-item-left">
              <ips-icon icon="ico-tick"></ips-icon>
            </div>
            <div class="dropdown-item-label">Dropdown label 2</div>
          </a>
        </template>
      </ips-dropdown>`,
});

const templateAvatar = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsDropdown, IpsAvatar, IpsButton },
  template: `<ips-dropdown :expanded="expanded" :classes="classes" :drop-right="dropRight">
    <template slot="trigger">
      <ips-avatar imageSrc="https://sguru.org/wp-content/uploads/2017/06/steam-avatar-profile-picture-0287.jpg">John Doe</ips-avatar>
      <span id="test-select-info" style="display:none">Arrow down for options.</span>
    </template>
    <template slot="items">
      <a class="dropdown-item" href="#">Action</a>
      <a class="dropdown-item" href="#">Another action</a>
      <a class="dropdown-item" href="#">Something else here</a>
    </template>
    </ips-dropdown>`,
});

const templateNested = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsDropdown, IpsButton },
  template: `<ips-dropdown :expanded="expanded">
    <template slot="trigger">
      <ips-button class="dropdown-toggle" id="dropdownMenuButton">Dropdown button</ips-button>
      <span id="parent-button-dropdown" style="display:none">Arrow down for options.</span>
    </template>
    <template slot="items">
      <a class="dropdown-item" href="#">Action</a>
      <a class="dropdown-item" href="#">Another action</a>
      <a class="dropdown-item" href="#">Something else here</a>
      <!-- Start nested child drop-down -->
      <ips-dropdown aria-described-by="childAriaRef">
        <template slot="trigger">
          <input class="dropdown-toggle" id="childDropDown"/>
          <span id="child-dropdown" style="display:none">Arrow down for child options.</span>
        </template>
        <template slot="items">
          <a class="dropdown-item" href>Child Action</a>
          <a class="dropdown-item" href>Another child action</a>
          <a class="dropdown-item" href>Something else here</a>
        </template>
      </ips-dropdown>
    </template>
    </ips-dropdown>`,
});

const templateCustomSelect = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsDropdown, IpsButton, IpsIcon },
  template: `<ips-dropdown :classes="classes" :drop-right="dropRight" :expanded="expanded">
    <template slot="trigger">
      <input id="selectDropDown" class="dropdown-toggle" type="text" autocomplete="off" v-model="selected"/>
      <ips-icon v-if="selected" icon="ico-delete" class="ips-form-icon--clear" @click.native="$emit('cleared', $event)"></ips-icon>
      <span id="testSelectInfo" style="display:none">Select a pause reason.</span>
    </template>
    <template v-slot:items="props">
      <span v-for="group in groupedOptions">
        <p class="dropdown-header text-start">{{ group.label }}</p>
        <a href v-for="opt in group.options" :class="{ active: opt.text === selected }" @click.prevent="makeChoice(opt.text, props.close)" class="dropdown-item">
          <ips-icon :icon="opt.icon"></ips-icon> {{ opt.text }}
        </a>
      </span>
    </template>
    </ips-dropdown>`,
});

export const basic = template.bind({});
basic.args = {};

export const select = templateSelect.bind({});
select.args = {};

export const avatar = templateAvatar.bind({});
avatar.args = {};

export const nested = templateNested.bind({});
nested.args = {};

export const customSelect = templateCustomSelect.bind({});
customSelect.args = {
  expanded: false,
  selected: 'Available',
  ariaRef: 'testSelectInfo',
  groupedOptions: [
    {
      label: 'Receive Calls',
      class: 'success',
      options: [{ icon: 'ico-phone', value: 'available', text: 'Available' }],
    },
    {
      label: 'Pause Calls',
      class: 'danger',
      options: [
        { icon: 'ico-pause', value: 'paused', text: 'Unavailable' },
        { icon: 'ico-pause', value: 'paused#1', text: 'Pause Reason' },
        { icon: 'ico-pause', value: 'paused#2', text: 'Pause Reason #2' },
      ],
    },
  ],
};
