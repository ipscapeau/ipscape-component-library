import { shallowMount } from '@vue/test-utils';
import IpsDropdown from './IpsDropdown.vue';

let wrapper;

describe('IpsDropdown.vue', () => {
  beforeEach(function () {
    wrapper = shallowMount(IpsDropdown);
  });

  test('Renders default dropdown', () => {
    expect(wrapper.find('div.dropdown').exists()).toBe(true);
  });
});
