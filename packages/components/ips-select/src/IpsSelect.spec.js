import { shallowMount } from '@vue/test-utils';
import IpsSelect from './IpsSelect.vue';

let wrapper;

const opts = [
  { value: 11, label: 'option 1' },
  { value: 12, label: 'option 2' },
  { value: 13, label: 'option 3' },
  { value: 14, label: 'option 4' },
];

describe('IpsSelect', () => {
  beforeEach(async () => {
    wrapper = shallowMount(IpsSelect, {
      template: `<div>
        <ips-select
          :id="id"
          :value="value"
          :classes="classes"
          :label-classes="labelClasses"
          :sizing="sizing"
          :max-height="maxHeight"
          :label="label"
          :display-label="displayLabel"
          :clear-on-focus="clearOnFocus"
          :placeholder="placeholder"
          :disabled="disabled"
          :required="required"
          :error="error"
          :tooltip="tooltip"
          :icon="icon"
          :iconAppend="iconAppend"
          :plainText="plainText"
          :clearable="clearable"
          :selected="selected"
          @select="onSelect">
          <template slot="options">
            <option
              v-for="(opt, i) in options"
              :key="i"
              :value="opt.value"
              :label="opt.label"
              :class="opt.class"
              :disabled="opt.disabled"
            >{{ opt.label }}</option>
          </template>
        </ips-select>
        <br/>
      </div>`,
      propsData: {
        id: 'select_id',
        label: 'select label',
        options: opts,
        value: opts[1].value,
      },
    });
  });

  test('renders a select element', () => {
    expect(wrapper.find('select').exists()).toBe(true);
  });

  test('should render a tooltip', async () => {
    await wrapper.setProps({ tooltip: 'Test tooltip' });
    const tip = wrapper.find('span[data-bs-toggle="tooltip"]');
    expect(tip).toBeDefined();
  });

  describe('field label', () => {
    test('should render and display a label', async () => {
      await wrapper.setProps({ label: 'this is a label', displayLabel: true });
      expect(wrapper.html()).toContain('label');
      expect(wrapper.html()).not.toContain('visually-hidden');
    });
    test('should render a label for screenreaders', async () => {
      expect(wrapper.html()).toContain('label');
      expect(wrapper.html()).toContain('visually-hidden');
    });
  });

  describe('Methods ', () => {
    describe('onSelect()', () => {
      test('should emit a "select" event with a value', () => {
        const event = {
          target: {
            value: 'Test',
          },
        };
        wrapper.vm.onSelect(event);
        // Will emit '12' as opts[1] is the selected option
        expect(wrapper.emitted().select[0]).toEqual([12]);
      });
    });
  });
});
