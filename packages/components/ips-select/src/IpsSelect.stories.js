import IpsSelect from '../src/IpsSelect.vue';

export default {
  title: 'Components/IpsSelect',
  component: IpsSelect,
  argTypes: {
    sizing: {
      control: {
        type: 'select',
        options: ['small', 'large'],
      },
    },
  },
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsSelect },
  template: `<div>
    <ips-select
      :id="id"
      :value="value"
      :classes="classes"
      :label-classes="labelClasses"
      :sizing="sizing"
      :max-height="maxHeight"
      :label="label"
      :display-label="displayLabel"
      :clear-on-focus="clearOnFocus"
      :placeholder="placeholder"
      :disabled="disabled"
      :required="required"
      :error="error"
      :tooltip="tooltip"
      :icon="icon"
      :iconAppend="iconAppend"
      :plainText="plainText"
      :clearable="clearable"
      :selected="selected"
      @select="onSelect">
      <template slot="options">
        <option
          v-for="(opt, i) in options"
          :key="i"
          :value="opt.value"
          :label="opt.label"
          :class="opt.class"
          :disabled="opt.disabled"
        >{{ opt.label }}</option>
      </template>
    </ips-select>
    <br/>
    <p>Selected Value: {{ selected }}</p>
  </div>`,
  methods: {
    onSelect(newValue) {
      this.selected = newValue;
    },
  },
});

export const basic = template.bind({});
basic.args = {
  id: 'basic',
  label: 'Basic select',
  value: 'AU',
  selected: 'AU',
  options: [
    { value: 'US', label: 'America' },
    { value: 'AL', label: 'Albania' },
    { value: 'AM', label: 'Armenia' },
    { value: 'AO', label: 'Angola' },
    { value: 'AQ', label: 'Antarctica' },
    { value: 'AR', label: 'Argentina' },
    { value: 'AU', label: 'Australia' },
    { value: 'BR', label: 'Brazil' },
  ],
};

export const placeholder = template.bind({});
placeholder.args = {
  id: 'with_placeholder',
  label: 'Select with Placeholder',
  selected: null,
  value: null,
  options: [
    {
      value: null,
      label: 'Select a country from the list...',
      class: 'placeholder',
      disabled: true,
    },
    { value: 'US', label: 'America' },
    { value: 'AL', label: 'Albania' },
    { value: 'AM', label: 'Armenia' },
    { value: 'AO', label: 'Angola' },
    { value: 'AQ', label: 'Antarctica' },
    { value: 'AR', label: 'Argentina' },
    { value: 'AU', label: 'Australia' },
    { value: 'BR', label: 'Brazil' },
  ],
};
