import IpsSpinner from '../src/IpsSpinner.vue';

export default {
  title: 'Components/IpsSpinner',
  component: IpsSpinner,
  argTypes: {
    size: {
      control: {
        type: 'select',
        options: ['sm', 'md', 'lg'],
      },
    },
    type: {
      control: {
        type: 'select',
        options: ['default', 'dots'],
      },
    },
  },
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsSpinner },
  template: '<ips-spinner :type="type" :show="show" :size="size" :text="text"></ips-spinner>',
});

export const basic = template.bind({});
basic.args = {};
