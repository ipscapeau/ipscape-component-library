import { shallowMount } from '@vue/test-utils';
import IpsSpinner from './IpsSpinner.vue';

describe('IpsSpinner.vue', () => {
  let wrapper;

  beforeEach(function () {
    wrapper = shallowMount(IpsSpinner);
  });

  test('Does not show spinner when show is false', async () => {
    const show = false;
    await wrapper.setProps({ show });
    expect(wrapper.html()).toBe('');
  });

  describe('Default spinner', () => {
    test('Renders default spinner', () => {
      expect(wrapper.html()).toContain('<div class="spinner">');
      expect(wrapper.html()).toContain('spinner-basic-style-md');
    });

    test('Renders small spinner', async () => {
      const show = true;
      const size = 'sm';
      await wrapper.setProps({ show, size });
      expect(wrapper.html()).toContain('<div class="spinner">');
      expect(wrapper.html()).toContain('spinner-basic-style-sm');
    });

    test('Renders medium spinner', async () => {
      const show = true;
      const size = 'md';
      await wrapper.setProps({ show, size });
      expect(wrapper.html()).toContain('<div class="spinner">');
      expect(wrapper.html()).toContain('spinner-basic-style-md');
    });

    test('Renders lg  spinner', async () => {
      const show = true;
      const size = 'lg';
      await wrapper.setProps({ show, size });
      expect(wrapper.html()).toContain('<div class="spinner">');
      expect(wrapper.html()).toContain('spinner-basic-style-lg');
    });
  });

  describe('Dots spinner', () => {
    test('Renders default dots spinner per default with all dots', async () => {
      const show = true;
      const type = 'dots';
      await wrapper.setProps({ show, type });
      expect(wrapper.html()).toContain('<div class="spinner">');
      expect(wrapper.html()).toContain('spinner-basic-style-md');
      expect(wrapper.html()).toContain('spinner-style-md');
      expect(wrapper.html()).toContain('spinner-dot-1');
      expect(wrapper.html()).toContain('spinner-dot-2');
      expect(wrapper.html()).toContain('spinner-dot-3');
      expect(wrapper.html()).toContain('spinner-dot-4');
    });

    test('Renders small dots spinner with all dots', async () => {
      const show = true;
      const size = 'sm';
      const type = 'dots';
      await wrapper.setProps({ show, size, type });
      expect(wrapper.html()).toContain('<div class="spinner">');
      expect(wrapper.html()).toContain('spinner-basic-style-sm');
      expect(wrapper.html()).toContain('spinner-style-sm');
      expect(wrapper.html()).toContain('spinner-dot-1');
      expect(wrapper.html()).toContain('spinner-dot-2');
      expect(wrapper.html()).toContain('spinner-dot-3');
      expect(wrapper.html()).toContain('spinner-dot-4');
    });

    test('Renders medium spinner with all dots', async () => {
      const show = true;
      const size = 'md';
      const type = 'dots';
      await wrapper.setProps({ show, size, type });
      expect(wrapper.html()).toContain('<div class="spinner">');
      expect(wrapper.html()).toContain('spinner-basic-style-md');
      expect(wrapper.html()).toContain('spinner-style-md');
      expect(wrapper.html()).toContain('spinner-dot-1');
      expect(wrapper.html()).toContain('spinner-dot-2');
      expect(wrapper.html()).toContain('spinner-dot-3');
      expect(wrapper.html()).toContain('spinner-dot-4');
    });

    test('Renders lg dots spinner with all dots', async () => {
      const show = true;
      const size = 'lg';
      const type = 'dots';
      await wrapper.setProps({ show, size, type });
      expect(wrapper.html()).toContain('<div class="spinner">');
      expect(wrapper.html()).toContain('spinner-basic-style-lg');
      expect(wrapper.html()).toContain('spinner-style-lg');
      expect(wrapper.html()).toContain('spinner-dot-1');
      expect(wrapper.html()).toContain('spinner-dot-2');
      expect(wrapper.html()).toContain('spinner-dot-3');
      expect(wrapper.html()).toContain('spinner-dot-4');
    });
  });
});
