import IpsCheckbox from '../src/IpsCheckbox.vue';

export default {
  title: 'Components/IpsCheckbox',
  component: IpsCheckbox,
  argTypes: {
    control: {
      control: {
        type: 'select',
        options: ['checkbox', 'radio', 'switch'],
      },
      defaultValue: 'checkbox',
    },
    sizing: {
      control: {
        type: 'select',
        options: ['small', 'large'],
      },
    },
  },
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsCheckbox },
  template: `<ips-checkbox
      :control="control"
      :group="group"
      :checked="checked"
      :init-value="initValue"
      :indeterminate="indeterminate"
      :id="id"
      :value="value"
      :classes="classes"
      :label-classes="labelClasses"
      :sizing="sizing"
      :max-height="maxHeight"
      :label="label"
      :display-label="displayLabel"
      :clear-on-focus="clearOnFocus"
      :placeholder="placeholder"
      :disabled="disabled"
      :required="required"
      :error="error"
      :tooltip="tooltip"
      :icon="icon"
      :iconAppend="iconAppend"
      :plainText="plainText"
      :clearable="clearable"><span slot="label">I accept the terms <br/> &amp; conditions</span></ips-checkbox>`,
});

const complexLabelTemplate = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsCheckbox },
  template: `
      <ips-checkbox
        :control="control"
        :group="group"
        :checked="checked"
        :init-value="initValue"
        :indeterminate="indeterminate"
        :id="id"
        :value="value"
        :classes="classes"
        :label-classes="labelClasses"
        :sizing="sizing"
        :max-height="maxHeight"
        :label="label"
        :display-label="displayLabel"
        :clear-on-focus="clearOnFocus"
        :placeholder="placeholder"
        :disabled="disabled"
        :required="required"
        :error="error"
        :tooltip="tooltip"
        :icon="icon"
        :iconAppend="iconAppend"
        :plainText="plainText"
        :clearable="clearable">
      <span slot="label_text"><b>Non-persistent connection.</b><br><span class="text-muted">Answer or decline incoming calls.</span></span></ips-checkbox>`,
});

export const complexLabel = complexLabelTemplate.bind({});
complexLabel.args = {
  id: 'complex',
  label: 'Test',
  displayLabel: true,
};

export const basic = template.bind({});
basic.args = {
  id: 'basic',
  label: 'checkbox label',
  displayLabel: true,
};

export const radio = template.bind({});
radio.args = {
  control: 'radio',
  id: 'radio',
  label: 'radio label',
  displayLabel: true,
};

export const toggle = template.bind({});
toggle.args = {
  control: 'switch',
  id: 'radio',
  label: 'toggle text',
  displayLabel: true,
};
