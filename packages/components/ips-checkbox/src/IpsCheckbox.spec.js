import { shallowMount } from '@vue/test-utils';
import IpsCheckbox from './IpsCheckbox.vue';

let wrapper;

describe('IpsCheckbox.vue', () => {
  beforeEach(() => {
    wrapper = shallowMount(IpsCheckbox, {
      propsData: {
        id: 'checkboxId',
        label: 'checkbox label',
        control: 'checkbox',
        disabled: false,
        required: true,
        checked: true,
      },
    });
  });

  test('has an input field', () => {
    expect(wrapper.find('input').exists()).toBe(true);
  });

  describe('checkbox field label', () => {
    test('should render and display a label', async () => {
      await wrapper.setProps({ label: 'this is a label', displayLabel: true });
      expect(wrapper.html()).toContain('label');
      expect(wrapper.html()).not.toContain('visually-hidden');
    });
    test('should render a label for screenreaders', async () => {
      expect(wrapper.html()).toContain('label');
      expect(wrapper.html()).toContain('visually-hidden');
    });
  });

  test('should render a checkbox if given control="checkbox"', () => {
    expect(wrapper.html()).toContain(
      '<input id="checkboxId" type="checkbox" role="checkbox" required="required" class="form-check-input" value="">'
    );
  });

  test('should render a checkbox if given control="switch"', async () => {
    await wrapper.setProps({ control: 'switch' });
    expect(wrapper.html()).toContain(
      '<input id="checkboxId" type="checkbox" role="switch" required="required" class="form-check-input" value="">'
    );
  });

  test('should render a radio button if given control="radio"', async () => {
    await wrapper.setProps({ control: 'radio', value: 'Test' });
    expect(wrapper.html()).toContain(
      '<input id="checkboxId" type="radio" role="radio" required="required" class="form-check-input" value="Test">'
    );
  });

  test('should be disabled if given prop', async () => {
    await wrapper.setProps({ disabled: true });
    const input = wrapper.find('input');
    expect(input.html()).toContain('disabled="disabled');
  });

  test('should render a tooltip', async () => {
    await wrapper.setProps({ tooltip: 'Test tooltip' });
    const tip = wrapper.find('span[data-bs-toggle="tooltip"]');
    expect(tip).toBeDefined();
  });

  describe('Computed props', () => {
    describe('isSwitch()', () => {
      test('should return a boolean', () => {
        expect(typeof wrapper.vm.isSwitch).toBe('boolean');
      });
    });

    describe('isRadio()', () => {
      test('should return a boolean', () => {
        expect(typeof wrapper.vm.isSwitch).toBe('boolean');
      });
    });

    describe('computedStyleClasses()', () => {
      test('should return an array', () => {
        expect(typeof wrapper.vm.computedStyleClasses).toBe('object');
      });

      test('should return "custom-radio" if "control" is "radio"', async () => {
        await wrapper.setProps({ control: 'radio' });
        const expected = ['form-check'];
        expect(wrapper.vm.computedStyleClasses).toEqual(expect.arrayContaining(expected));
      });

      test('should return "custom-checkbox" if "control" is "checkbox"', async () => {
        await wrapper.setProps({ control: 'checkbox' });
        const expected = ['form-check'];
        expect(wrapper.vm.computedStyleClasses).toEqual(expect.arrayContaining(expected));
      });
    });

    describe('inputType()', () => {
      test('should return "radio" if the prop control === "radio"', async () => {
        await wrapper.setProps({ control: 'radio' });
        expect(wrapper.vm.inputType).toBe('radio');
      });

      test('should return "checkbox" if the prop control !== "radio"', async () => {
        await wrapper.setProps({ control: 'switch' });
        expect(wrapper.vm.inputType).toBe('checkbox');
      });
    });
  });

  describe('Methods ', () => {
    describe('update()', () => {
      test('should emit a "change" event with the value prop if "control" is "radio"', async () => {
        await wrapper.setProps({ control: 'radio', value: 'Test' });
        wrapper.find('input').trigger('change');
        expect(wrapper.emitted().change[0]).toEqual(['Test']);
      });

      test('should emit a "change" event with true/false if "control" is "checkbox"', async () => {
        await wrapper.setProps({ control: 'checkbox' });
        wrapper.find('input').trigger('change');
        expect(wrapper.emitted().change[0]).toEqual([true]);
      });
    });
  });
});
