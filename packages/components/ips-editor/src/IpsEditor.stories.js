import IpsEditor from './IpsEditor.vue';

export default {
  title: 'Components/IpsEditor',
  component: IpsEditor,
  argTypes: {
    dynamicFields: {
      type: Array,
      defaultValue: [
        { name: '%FIELD_4ET%', label: 'Order Name' },
        { name: '%FIELD_7ET%', label: 'Order Telephone Number' },
        { name: '%FIELD_5ET%', label: 'Order Date' },
        { name: '%FIELD_9ET%', label: 'Order Delivery Address' },
      ],
    },
    systemVariables: {
      type: Array,
      defaultValue: [
        { name: '[agent_last_name]', label: 'Agent last name' },
        { name: '[agent_first_name]', label: 'Agent first name' },
        { name: '[organisation_title]', label: 'Organisation title' },
        { name: '[lead_id]', label: 'Lead id' },
      ],
    },
  },
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsEditor },
  template:
    '<ips-editor :dynamic-fields="dynamicFields" :system-variables="systemVariables"></ips-editor>',
});

export const basic = template.bind({});
basic.args = {};
