import { createLocalVue, mount } from '@vue/test-utils';
import IpsSidebar from './IpsSidebar.vue';
import VueRouter from 'vue-router';

const localVue = createLocalVue();
localVue.use(VueRouter);
const router = new VueRouter();

describe('IpsSidebar.vue', () => {
  let wrapper;

  beforeEach(function () {
    wrapper = mount(IpsSidebar, {
      localVue,
      router,
      propsData: {},
    });
  });

  test('Renders default sidebar without items', () => {
    expect(wrapper.html()).toContain('<div id="sidebarContainer"');
  });

  test('Renders default sidebar with items', async () => {
    await wrapper.setProps({
      items: [
        {
          id: 'item',
          name: 'Item',
          tip: 'Item',
          icon: 'ico-document',
          href: 'item',
          badgeCount: '22',
        },
        {
          id: 'test',
          name: 'Test',
          tip: 'Test',
          icon: 'ico-document',
          href: 'test',
        },
      ],
    });
    expect(wrapper.html()).toContain(
      '<div id="sidebarContainer" role="navigation" class="d-none d-md-block h-100 sidebar-collapsed">'
    );
  });

  test('Renders default sidebar with bottom items', async () => {
    expect(wrapper.html()).not.toContain('ico-help');
    await wrapper.setProps({
      items: [
        {
          id: 'test',
          name: 'Test',
          tip: 'Test',
          icon: 'ico-help',
          href: 'test',
        },
      ],
    });
    expect(wrapper.html()).toContain('ico-help');
  });

  test('Emits click on item select', async () => {
    await wrapper.setProps({
      items: [
        {
          id: 'item',
          name: 'Item',
          tip: 'Item',
          icon: 'ico-document',
          href: 'item',
          badgeCount: '22',
        },
        {
          id: 'test',
          name: 'Test',
          tip: 'Test',
          icon: 'ico-document',
          href: 'test',
        },
      ],
    });
    await wrapper.find('[name=item]').trigger('click');
  });

  test('Emits click on collapse sidebar', async () => {
    await wrapper.setProps({
      items: [
        {
          id: 'item',
          name: 'Item',
          tip: 'Item',
          icon: 'ico-document',
          href: 'item',
          badgeCount: '22',
        },
        {
          id: 'test',
          name: 'Test',
          tip: 'Test',
          icon: 'ico-document',
          href: 'test',
        },
      ],
    });
    await wrapper.find('#toggleSidebar').trigger('click');
  });
});
