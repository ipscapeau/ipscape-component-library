import IpsSidebar from '../src/IpsSidebar.vue';
import IpsIcon from '../../ips-icon/src/IpsIcon.vue';

export default {
  title: 'Components/IpsSidebar',
  component: IpsSidebar,
  argTypes: {
    items: {
      defaultValue: [
        {
          id: 'item',
          name: 'Item',
          tip: 'Item',
          icon: 'ico-phone',
          href: 'item',
          badgeCount: '22',
        },
        {
          id: 'test',
          name: 'Test',
          tip: 'Test',
          icon: 'ico-chat',
          href: 'test',
        },
        {
          id: 'components',
          name: 'Components',
          tip: 'components',
          icon: 'ico-component',
          href: 'components',
          items: [
            {
              id: 'components.alert',
              name: 'Alert',
              tip: 'Alert',
              href: 'alert',
            },
            {
              id: 'components.badge',
              name: 'Badge',
              tip: 'Badge',
              href: 'badge',
            },
          ],
        },
      ],
    },
  },
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsSidebar, IpsIcon },
  template:
    '<ips-sidebar :items="items" :logo="logo" :expanded="expanded" :expandable="expandable" :homepage-link="homepageLink" selected-item="selectedItem" @click.stop><span slot="logo"><ips-icon icon="ico-ipscape-stroke"></ips-icon></span></ips-sidebar>',
});

export const basic = template.bind({});
basic.args = {
  id: 'basic',
};
