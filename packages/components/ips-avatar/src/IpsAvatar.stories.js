import IpsAvatar from '../src/IpsAvatar.vue';

export default {
  title: 'Components/IpsAvatar',
  component: IpsAvatar,
  argTypes: {
    initials: {
      control: {
        type: 'text',
      },
      defaultValue: 'WM',
    },
    size: {
      control: {
        type: 'select',
        options: ['sm', 'md', 'lg'],
      },
    },
  },
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsAvatar },
  template: '<ips-avatar :image-src="imageSrc" :size="size" >{{initials}}</ips-avatar>',
});

export const basic = template.bind({});
basic.args = {
  imageSrc: undefined,
  size: '',
};
