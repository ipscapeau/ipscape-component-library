import { shallowMount } from '@vue/test-utils';
import IpsAvatar from './IpsAvatar.vue';

let wrapper;

describe('IpsAvatar.vue', () => {
  beforeEach(function () {
    wrapper = shallowMount(IpsAvatar);
  });

  test('Renders default (medium) avatar', () => {
    expect(wrapper.html()).toContain('avatar md');
  });

  test('Renders small avatar', async () => {
    const size = 'sm';
    await wrapper.setProps({ size });
    expect(wrapper.html()).toContain('avatar sm');
  });

  test('Renders large avatar', async () => {
    const size = 'lg';
    await wrapper.setProps({ size });
    expect(wrapper.html()).toContain('avatar lg');
  });

  test('Renders image avatar', async () => {
    const imageSrc = 'imageURL';
    await wrapper.setProps({ imageSrc });
    expect(wrapper.html()).toContain(`background-image: url(${imageSrc})`);
  });
});
