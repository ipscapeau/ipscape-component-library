import { shallowMount } from '@vue/test-utils';
import IpsTheme from './IpsTheme.vue';

let wrapper;

const themeOpts = {
  primary: 'red',
  secondary: 'blue',
  tertiary: 'green',
};

describe('IpsTheme.vue', () => {
  beforeEach(async () => {
    wrapper = shallowMount(IpsTheme);
  });

  test('is a Vue instance', () => {
    expect(wrapper.isVueInstance).toBeTruthy();
  });

  test('should have a theme', () => {
    expect(wrapper.vm.theme).toBeDefined();
  });

  test('should fallback to the default color scheme if no theme is supplied', () => {
    expect(wrapper.vm.theme.primary).toEqual('#f66a0a');
  });

  test('should have a "primary" color', async () => {
    await wrapper.setProps({ theme: themeOpts });
    expect(wrapper.vm.theme.primary).toEqual(themeOpts.primary);
  });
});
