import IpsButton from '../src/IpsButton.vue';
import IpsTheme from '../../ips-theme/src/IpsTheme.vue';

export default {
  title: 'Components/IpsButton',
  component: IpsButton,
  argTypes: {
    variant: {
      control: {
        type: 'select',
        options: ['primary', 'secondary', 'tertiary', 'minimal', 'success', 'danger', 'link'],
      },
      defaultValue: 'success',
    },
    size: {
      control: {
        type: 'select',
        options: ['xxs', 'xs', 'sm', 'md', 'lg', 'block'],
      },
      defaultValue: 'md',
    },
    type: {
      control: {
        type: 'select',
        options: ['button', 'reset', 'submit'],
      },
      defaultValue: 'button',
    },
    loading: {
      control: {
        type: 'select',
        options: [true, false],
      },
      defaultValue: false,
    },
  },
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsButton, IpsTheme },
  template: '<ips-theme><ips-button v-bind="$props">{{ label }}</ips-button></ips-theme>',
});

export const basic = template.bind({});
basic.args = {
  label: 'Default button',
};

export const circle = template.bind({});
circle.args = {
  label: null,
  circle: true,
  icon: 'ico-phone',
};

export const iconPrepend = template.bind({});
iconPrepend.args = {
  label: 'Icon prepend',
  icon: 'ico-phone',
};

export const iconAppend = template.bind({});
iconAppend.args = {
  label: 'Icon append',
  iconAppend: 'ico-phone',
};

export const link = template.bind({});
link.args = {
  label: 'Link button',
  variant: 'link',
};

const complexTemplate = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsButton, IpsTheme },
  template: `
    <ips-button
        id="callInButton"
        class="w-25 connect-voice-button text-start"
        variant="tertiary"
        icon="ico-recipient"
        icon-append="ico-chevron-right"
        align-text="left"
    >Call in to connect</ips-button
    >`,
});

export const complex = complexTemplate.bind({});
complex.args = {
  label: 'Link button',
  variant: 'tertiary',
};
