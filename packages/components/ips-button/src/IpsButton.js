import styled from 'vue-styled-components';
import tinycolor from 'tinycolor2';

const getRGBA = (color, alpha) => {
  return color.setAlpha(alpha).toString();
};

const setContrastColor = (color, lightSetting) => {
  return color.isDark() ? '#ffffff' : color.lighten(lightSetting).toString();
};

const StyledButton = styled('button', { variant: String })`
  background-color: ${(props) =>
    props.variant !== 'tertiary' ? props.theme[props.variant] : '#ffffff'};
  border-color: ${(props) =>
    props.variant !== 'tertiary'
      ? tinycolor(props.theme[props.variant]).darken(1).toString()
      : tinycolor(props.theme[props.variant]).lighten(30).toString()};
  color: ${(props) =>
    props.variant !== 'tertiary'
      ? setContrastColor(tinycolor(props.theme[props.variant]), 80)
      : props.theme[props.variant]};

  &:hover {
    background-color: ${(props) =>
      props.variant !== 'tertiary'
        ? getRGBA(tinycolor(props.theme[props.variant]), 0.8)
        : '#ffffff'};
    color: ${(props) =>
      props.variant !== 'tertiary'
        ? tinycolor(props.theme[props.variant]).lighten(80).toString()
        : props.theme[props.variant]};
    border-color: ${(props) =>
      props.variant !== 'tertiary'
        ? setContrastColor(tinycolor(props.theme[props.variant]), 80)
        : tinycolor(props.theme[props.variant]).darken(5).toString()};
  }

  &.focus,
  &:focus {
    ${(props) =>
      props.variant !== 'tertiary'
        ? 'box-shadow: 0 0 0 3px ' + getRGBA(tinycolor(props.theme[props.variant]).lighten(30), 0.8)
        : 'box-shadow: 0 0 0 3px ' + getRGBA(tinycolor(props.theme.primary).lighten(35), 0.8)};
    background-color: ${(props) =>
      props.variant !== 'tertiary' ? props.theme[props.variant] : '#ffffff'};
    border-color: ${(props) =>
      props.variant !== 'link'
        ? tinycolor(props.theme[props.variant]).lighten(30).toString()
        : 'transparent'};
    color: ${(props) =>
      props.variant !== 'tertiary'
        ? tinycolor(props.theme[props.variant]).lighten(80).toString()
        : props.theme[props.variant]};
  }

  &.disabled,
  &:disabled {
    background-color: ${(props) =>
      props.variant !== 'tertiary'
        ? getRGBA(tinycolor(props.theme[props.variant]), 0.5)
        : '#ffffff'};
    border-color: ${(props) =>
      props.variant !== 'tertiary'
        ? 'transparent'
        : tinycolor(props.theme[props.variant]).lighten(30).toString()};
    color: ${(props) =>
      props.variant !== 'tertiary'
        ? tinycolor(props.theme[props.variant]).lighten(90).toString()
        : tinycolor(props.theme[props.variant]).lighten(30).toString()};
  }

  &:not(:disabled):not(.disabled):active,
  &:not(:disabled):not(.disabled).active,
  .show > .dropdown-toggle {
    background-color: ${(props) =>
      props.variant !== 'tertiary'
        ? tinycolor(props.theme[props.variant]).darken(5).toString()
        : 'transparent'};
    border-color: ${(props) =>
      props.variant !== 'tertiary'
        ? tinycolor(props.theme[props.variant]).darken(3).toString()
        : tinycolor(props.theme.primary).darken(5).toString()};
    color: ${(props) =>
      props.variant !== 'tertiary'
        ? tinycolor(props.theme[props.variant]).lighten(80).toString()
        : tinycolor(props.theme[props.variant]).darken(5).toString()};
  }

  &.btn-link {
    color: ${(props) => props.theme.primary};
    border: none !important;
    background-color: transparent;
    &:hover {
      background-color: transparent;
      border: none;
    }
    &:focus,
    &.focus {
      box-shadow: none;
      background-color: transparent;
      border: none;
    }
    &:not(:disabled):not(.disabled):active,
    &:not(:disabled):not(.disabled).active,
    .show {
      box-shadow: none;
      background-color: transparent;
      color: ${(props) => tinycolor(props.theme.primary).toString()};
    }
    &.disabled,
    &:disabled {
      background-color: transparent;
      border: none;
      color: ${(props) => tinycolor(props.theme.primary).lighten(10).toString()};
    }
  }
`;

export default StyledButton;
