import { shallowMount } from '@vue/test-utils';
import IpsButton from './IpsButton.vue';

let wrapper;

describe('IpsButton.vue', () => {
  beforeEach(function () {
    wrapper = shallowMount(IpsButton);
  });

  test('Renders default button', () => {
    expect(wrapper.html()).toEqual(expect.stringContaining('type="button"'));
    expect(wrapper.html()).toContain('btn-primary');
    expect(wrapper.html()).toContain('btn-md');
  });

  test('Renders primary button', async () => {
    const variant = 'primary';
    await wrapper.setProps({ variant });
    expect(wrapper.html()).toContain('btn-primary');
  });

  test('Renders secondary button', async () => {
    const variant = 'secondary';
    await wrapper.setProps({ variant });
    expect(wrapper.html()).toContain('btn-secondary');
  });

  test('Renders tertiary button', async () => {
    const variant = 'tertiary';
    await wrapper.setProps({ variant });
    expect(wrapper.html()).toContain('btn-tertiary');
  });

  test('Renders success button', async () => {
    const variant = 'success';
    await wrapper.setProps({ variant });
    expect(wrapper.html()).toContain('btn-success');
  });

  test('Renders danger button', async () => {
    const variant = 'danger';
    await wrapper.setProps({ variant });
    expect(wrapper.html()).toContain('btn-danger');
  });

  test('Renders link button', async () => {
    const variant = 'link';
    await wrapper.setProps({ variant });
    expect(wrapper.html()).toContain('btn-link');
  });

  test('Renders disabled button', async () => {
    const disabled = true;
    await wrapper.setProps({ disabled });
    expect(wrapper.html()).toEqual(expect.stringContaining('disabled'));
  });

  test('Renders extra small button', async () => {
    const size = 'xxs';
    await wrapper.setProps({ size });
    expect(wrapper.html()).toContain('btn-xxs');
  });

  test('Renders extra small button', async () => {
    const size = 'xs';
    await wrapper.setProps({ size });
    expect(wrapper.html()).toContain('btn-xs');
  });

  test('Renders small button', async () => {
    const size = 'sm';
    await wrapper.setProps({ size });
    expect(wrapper.html()).toContain('btn-sm');
  });

  test('Renders medium button', async () => {
    const size = 'md';
    await wrapper.setProps({ size });
    expect(wrapper.html()).toContain('btn-md');
  });

  test('Renders large button', async () => {
    const size = 'lg';
    await wrapper.setProps({ size });
    expect(wrapper.html()).toContain('btn-lg');
  });

  test('Renders block button', async () => {
    const size = 'block';
    await wrapper.setProps({ size });
    expect(wrapper.html()).toContain('btn-block');
  });

  test('Renders button-type button', async () => {
    const type = 'button';
    await wrapper.setProps({ type });
    expect(wrapper.html()).toContain('type="button"');
  });

  test('Renders reset-type button', async () => {
    const type = 'reset';
    await wrapper.setProps({ type });
    expect(wrapper.html()).toContain('type="reset"');
  });

  test('Renders submit-type button', async () => {
    const type = 'submit';
    await wrapper.setProps({ type });
    expect(wrapper.html()).toContain('type="submit"');
  });

  test('Renders circle button', async () => {
    const circle = true;
    await wrapper.setProps({ circle });
    expect(wrapper.html()).toContain('btn-circle');
  });

  test('Renders button with icon', async () => {
    const icon = 'ico-star';
    await wrapper.setProps({ icon });
    expect(wrapper.html()).toContain(icon);
  });

  test('Renders button with append icon', async () => {
    const iconAppend = 'ico-star';
    await wrapper.setProps({ iconAppend });
    expect(wrapper.html()).toContain(iconAppend);
  });

  test('Renders button with prepend icon and append icon', async () => {
    const icon = 'ico-star';
    const iconAppend = 'ico-star';
    await wrapper.setProps({ icon, iconAppend });
    expect(wrapper.html()).toContain(icon);
    expect(wrapper.html()).toContain(iconAppend);
  });

  test('Aligns text center', async () => {
    const alignText = 'center';
    await wrapper.setProps({ alignText });
    expect(wrapper.html()).not.toContain('float-end');
    expect(wrapper.html()).not.toContain('float-start');
  });

  test('Renders loading text', async () => {
    const loading = true;
    const loadingText = 'This is the loading text';
    await wrapper.setProps({ loading, loadingText });
    expect(wrapper.html()).toContain(loadingText);
  });

  test('Does not render loading text', async () => {
    const loading = false;
    const loadingText = 'This is the loading text';
    await wrapper.setProps({ loading, loadingText });
    expect(wrapper.html()).not.toContain(loadingText);
  });

  test('Emits button click event', () => {
    wrapper.find('styledbutton-stub').trigger('click');
  });
});
