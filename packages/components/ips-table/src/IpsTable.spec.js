import { mount } from '@vue/test-utils';
import IpsTable from './IpsTable.vue';

describe('IpsTable.vue', () => {
  let wrapper;

  beforeEach(function () {
    wrapper = mount(IpsTable, {
      propsData: {
        type: 'bordered',
        rows: [
          {
            id: 1,
            name: 'John Doe',
            state: 'Active',
            time: '00:30:00',
            lastLogin: 'March 10, 2019 10:35.04',
            checked: true,
          },
        ],
        columns: [
          {
            label: 'Id',
            key: 'id',
            sortable: true,
          },
        ],
      },
    });
  });

  test('Renders bordered table', () => {
    const tableHtml = wrapper.find('table').html();
    expect(tableHtml).toContain('class="ips table w-auto mw-100 bordered table-bordered"');
  });
});
