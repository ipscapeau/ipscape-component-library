import IpsTable from '../src/IpsTable.vue';

export default {
  title: 'Components/IpsTable',
  component: IpsTable,
  argTypes: {
    type: {
      control: {
        type: 'select',
        options: ['bordered', 'striped', 'dark', 'hover'],
      },
      defaultValue: 'hover',
    },
    headType: {
      control: {
        type: 'select',
        options: ['dark', 'light'],
      },
    },
    columns: {
      defaultValue: [
        {
          classes: [],
          headerClasses: [],
          key: 'id',
          label: 'Id',
          sortable: true,
          style: '',
          type: 'text',
        },
        {
          classes: [],
          headerClasses: [],
          key: 'name',
          label: 'Name',
          sortable: true,
          style: '',
          type: 'text',
        },
        {
          classes: [],
          headerClasses: [],
          key: 'state',
          label: 'State',
          sortable: true,
          style: '',
          type: 'text',
        },
        {
          classes: [],
          headerClasses: [],
          key: 'time',
          label: 'Time',
          sortable: true,
          style: '',
          type: 'text',
        },
        {
          classes: [],
          headerClasses: [],
          key: 'lastLogin',
          label: 'Last login',
          sortable: true,
          style: '',
          type: 'text',
        },
      ],
    },
    rows: {
      defaultValue: [
        {
          id: 12,
          name: 'John Doe',
          state: 'Active',
          time: '00:30:00',
          lastLogin: 'March 10, 2019 10:35.04',
          checked: true,
        },
        {
          id: 22,
          name: 'Jane Doe',
          state: 'Paused',
          time: '00:00:30',
          lastLogin: 'March 10, 2019 10:00.24',
          checked: false,
        },
        {
          id: 99,
          name: 'Johnny Appleseed',
          state: 'Talk',
          time: '00:01:30',
          lastLogin: 'March 10, 2019 12:00.00',
          checked: true,
        },
      ],
    },
  },
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsTable },
  template:
    '<ips-table id="tableStory" :type="type" :head-type="headType" :columns="columns" :rows="rows"' +
    ' :selectable="selectable"' +
    ' :editable="editable" :pagination="pagination"></ips-table>',
});

export const basic = template.bind({});
basic.args = {};
