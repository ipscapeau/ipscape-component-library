import IpsInput from '../src/IpsInput.vue';

export default {
  title: 'Components/IpsInput',
  component: IpsInput,
  argTypes: {
    type: {
      control: {
        type: 'select',
        options: ['text', 'password', 'number', 'search', 'email'],
      },
      defaultValue: 'text',
    },
    sizing: {
      control: {
        type: 'select',
        options: ['small', 'large'],
      },
    },
  },
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsInput },
  template:
    '<ips-input v-model="value" :type="type" :password-reveal="passwordReveal" :id="id" :classes="classes" :label-classes="labelClasses" :sizing="sizing" :max-height="maxHeight" :label="label" :display-label="displayLabel" :clear-on-focus="clearOnFocus" :placeholder="placeholder" :disabled="disabled" :required="required" :error="error" :tooltip="tooltip" :icon="icon" :iconAppend="iconAppend" :plainText="plainText" :clearable="clearable"></ips-input>',
});

export const basic = template.bind({});
basic.args = {
  id: 'basic',
  label: 'basic label',
};

export const icon = template.bind({});
icon.args = {
  id: 'icon',
  label: 'icon label',
  icon: 'ico-phone',
  iconAppend: 'ico-calendar',
};

export const number = template.bind({});
number.args = {
  id: 'number',
  label: 'number label',
  type: 'number',
};

export const password = template.bind({});
password.args = {
  id: 'password',
  label: 'password label',
  type: 'password',
  passwordReveal: true,
  value: 'password',
};

export const placeholder = template.bind({});
placeholder.args = {
  id: 'placeholder',
  label: 'placeholder label',
  placeholder: 'This is a placeholder',
};

export const required = template.bind({});
required.args = {
  id: 'required',
  label: 'required label',
  required: true,
};

export const search = template.bind({});
search.args = {
  id: 'search',
  label: 'search label',
  type: 'search',
};

export const tooltip = template.bind({});
tooltip.args = {
  id: 'tooltip',
  label: 'tooltip label',
  tooltip: 'This is a tooltip',
};

export const validation = template.bind({});
validation.args = {
  id: 'validation',
  label: 'validation label',
  error: true,
};
