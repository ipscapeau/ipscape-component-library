import { mount } from '@vue/test-utils';
import IpsInput from './IpsInput.vue';

let wrapper;

describe('IpsInput', () => {
  beforeEach(() => {
    wrapper = mount(IpsInput, {
      data: () => {
        return {
          isDirty: true,
        };
      },
      propsData: { id: 'defauldId', label: 'this is a label' },
    });
  });

  describe('Computed', () => {
    describe('isSearchType', () => {
      test('should return true a boolean', async () => {
        await wrapper.setProps({ type: 'search' });
        const searchType = wrapper.vm.isSearchType;
        expect(typeof searchType).toBe('boolean');
      });

      test('should return true if prop type === search', async () => {
        await wrapper.setProps({ type: 'search' });
        const searchType = wrapper.vm.isSearchType;
        expect(searchType).toBeTruthy();
      });

      test('should return "false" if prop type !== search', async () => {
        await wrapper.setProps({ type: 'text' });
        const searchType = wrapper.vm.isSearchType;
        expect(searchType).toBeFalsy();
      });
    });

    describe('formComputedClasses()', () => {
      test('should return an array', () => {
        expect(typeof wrapper.vm.formComputedClasses).toBe('object');
      });
    });

    describe('labelComputedClasses()', () => {
      test('should return an array', () => {
        expect(typeof wrapper.vm.labelComputedClasses).toBe('object');
      });

      test('that matches even if received contains additional elements', () => {
        const expected = ['ips'];
        expect(wrapper.vm.labelComputedClasses).toEqual(expect.arrayContaining(expected));
      });

      test('should include "visually-hidden" if "label" is set to false', async () => {
        await wrapper.setProps({ displayLabel: false });
        const expected = ['visually-hidden'];
        expect(wrapper.vm.labelComputedClasses).toEqual(expect.arrayContaining(expected));
      });

      test('should include "has-validation" if "error" is set', async () => {
        await wrapper.setProps({ error: true });
        const expected = ['has-validation'];
        expect(wrapper.vm.labelComputedClasses).toEqual(expect.arrayContaining(expected));
      });

      test('should include "is-required" if "required" is set', async () => {
        await wrapper.setProps({ required: true });
        const expected = ['is-required'];
        expect(wrapper.vm.labelComputedClasses).toEqual(expect.arrayContaining(expected));
      });
    });

    describe('controlSizing()', () => {
      test('should return a string when passed a prop value', async () => {
        await wrapper.setProps({ sizing: 'small' });
        expect(typeof wrapper.vm.controlSizing).toBe('string');
      });
    });

    describe('inputComputedClasses()', () => {
      test('should return an array', () => {
        expect(typeof wrapper.vm.inputComputedClasses).toBe('object');
      });

      test('should include a "sizing" value when prop is set', async () => {
        await wrapper.setProps({ sizing: 'small' });
        const expected = ['form-control-sm'];
        expect(wrapper.vm.inputComputedClasses).toEqual(expect.arrayContaining(expected));
      });

      test('should return "is-invalid" if prop "error" is true', async () => {
        await wrapper.setProps({ error: true });
        const expected = ['is-invalid'];
        expect(wrapper.vm.inputComputedClasses).toEqual(expect.arrayContaining(expected));
      });
    });
  });

  test('renders the correct default markup', () => {
    const inputHtml = wrapper.find('input').html();
    expect(inputHtml).toContain(
      '<input id="defauldId" type="text" autocomplete="on" class="form-control ips">'
    );
  });

  test('has an input field', () => {
    expect(wrapper.find('input').exists()).toBe(true);
  });

  test('should render a placeholder if given prop', async () => {
    await wrapper.setProps({ placeholder: 'Foo' });
    const inputHtml = wrapper.find('input').html();
    expect(inputHtml).toContain('placeholder="Foo"');
  });

  describe('input field label', () => {
    test('should render and display a label', async () => {
      await wrapper.setProps({ label: 'this is a label', displayLabel: true });
      expect(wrapper.html()).toContain('label');
      expect(wrapper.html()).not.toContain('visually-hidden');
    });
    test('should render a label for screenreaders', async () => {
      expect(wrapper.html()).toContain('label');
      expect(wrapper.html()).toContain('visually-hidden');
    });
  });

  test('should be disabled if given prop', async () => {
    await wrapper.setProps({ disabled: true });
    const input = wrapper.find('input');
    expect(input.html()).toContain('disabled="disabled');
  });

  test('should display validation state', async () => {
    await wrapper.setProps({ error: true });
    const input = wrapper.find('input');
    expect(input.html()).toContain('class="form-control ips is-invalid"');
  });

  test('renders search input', async () => {
    await wrapper.setProps({ type: 'search' });
    const input = wrapper.find('input');
    expect(input.html()).toContain('type="search"');
  });

  test('search input contains search', async () => {
    await wrapper.setProps({ type: 'search' });
    expect(wrapper.html()).toContain('ico-search');
  });

  test('search input contains clear', async () => {
    await wrapper.setProps({ type: 'search', clearable: true });
    wrapper.find('input').setValue('Test Search');
    const icon = wrapper.find('i.ico-close');
    expect(icon).toBeDefined();
  });

  test('renders number input', async () => {
    await wrapper.setProps({ type: 'number' });
    const input = wrapper.find('input');
    expect(input.html()).toContain('type="number"');
  });

  test('should render a tooltip', async () => {
    await wrapper.setProps({ tooltip: 'Test tooltip' });
    const tip = wrapper.find('span[data-bs-toggle="tooltip"]');
    expect(tip).toBeDefined();
  });

  test('Emits input event', () => {
    wrapper.find('input').trigger('input');
  });

  test('Emits clear input click event', async () => {
    await wrapper.setProps({ type: 'search' });
    wrapper.findAll('i').trigger('click');
  });
});
