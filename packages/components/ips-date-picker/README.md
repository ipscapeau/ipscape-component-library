# ips-date-picker

[![tested with jest](https://img.shields.io/badge/tested_with-jest-99424f.svg)](https://github.com/facebook/jest) [![jest](https://jestjs.io/img/jest-badge.svg)](https://github.com/facebook/jest)

The `<ips-date-picker>` provides a date picker component.

## Live demo

See [Storybook](http://ipscape-component-library.s3-website-ap-southeast-2.amazonaws.com/storybook/)

## Getting started

Install the component library:

```
npm install @ipscape/ips-date-picker
```

or

```
yarn add @ipscape/ips-date-picker
```

Add the component to your app:

```javascript
import Vue from 'vue';
import IpsDatePicker from '@ipscape/ips-date-picker';
import '@ipscape/ips-date-picker/dist/ips-date-picker.css';

Vue.component(IpsDatePicker.name, IpsDatePicker);
```

Use a component in your application:

```html
<template>
  <ips-date-picker></ips-date-picker>
</template>
```

## License

[![License: GPL v2](https://img.shields.io/badge/License-GPL%20v2-blue.svg)](https://img.shields.io/badge/License-GPL%20v2-blue.svg) [![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

Made with &#x2764; by [ipSCAPE](https://ipscape.com)

Licensed under the terms of [GNU General Public License Version 2 or later](http://www.gnu.org/licenses/gpl.html). For full details about the license, please check the `LICENSE.md` file.
