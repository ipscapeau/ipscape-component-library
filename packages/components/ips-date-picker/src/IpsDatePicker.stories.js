import IpsDatePicker from '../src/IpsDatePicker.vue';

export default {
  title: 'Components/IpsDatePicker',
  component: IpsDatePicker,
  argTypes: {
    mode: {
      control: {
        type: 'select',
        options: ['date', 'dateTime', 'time'],
      },
    },
  },
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsDatePicker },
  template: `<div><ips-date-picker :id="id" :label="label" :mode="mode" :timezone="timezone" :min-date="minDate" :max-date="maxDate" :is24hr="is24hr" :is-expanded="isExpanded" :is-range="isRange"></ips-date-picker>{{selectedDate}}</div>`,
});

export const basic = template.bind({});
basic.args = { id: 'basic', label: 'Basic' };

export const dateTime = template.bind({});
dateTime.args = { id: 'dateTime', label: 'Date and time', mode: 'dateTime' };

export const time = template.bind({});
time.args = { id: 'time', label: 'Time', mode: 'time' };

export const time24hr = template.bind({});
time24hr.args = { id: 'time24hr', label: 'Time 24 hours', mode: 'time', is24hr: true };

export const minDate = template.bind({});
minDate.args = { id: 'minDate', label: 'Minimum date', minDate: new Date() };

export const maxDate = template.bind({});
maxDate.args = { id: 'maxDate', label: 'Maximum date', maxDate: new Date() };

export const isExpanded = template.bind({});
isExpanded.args = { id: 'isExpanded', label: 'Expanded', isExpanded: true };

export const isRange = template.bind({});
isRange.args = { id: 'isRange', label: 'Date range', isRange: true };
