import { mount } from '@vue/test-utils';
import IpsDatePicker from './IpsDatePicker.vue';

let wrapper;

describe('IpsDatePicker.vue', () => {
  beforeEach(() => {
    wrapper = mount(IpsDatePicker, {});
  });

  test('Renders date picker popover content wrapper', () => {
    expect(wrapper.find('div.vc-popover-content-wrapper').exists()).toBe(true);
  });
});

// describe('IpsDatetimePicker', () => {
//   beforeEach(() => {
//     wrapper = mount(IpsDatetimePicker, {
//       propsData: {
//         id: 'dateTimePicker',
//         label: 'label',
//       },
//     });
//   });
//
//   test('Renders default datetime picker', () => {
//     expect(wrapper.html()).toContain('id="dateTimePicker"');
//     expect(wrapper.html()).toContain('dateformat="d.m.Y H:i"');
//     expect(wrapper.html()).toContain('stepminutes="15"');
//     expect(wrapper.html()).toContain('locale="en"');
//   });
//
//   describe('datetime field label', () => {
//     test('should render and display a label', async () => {
//       await wrapper.setProps({ label: 'this is a label', displayLabel: true });
//       expect(wrapper.html()).toContain('label');
//       expect(wrapper.html()).not.toContain('visually-hidden');
//     });
//     test('should render a label for screenreaders', async () => {
//       expect(wrapper.html()).toContain('label');
//       expect(wrapper.html()).toContain('visually-hidden');
//     });
//   });
//
//   test('Renders datetime picker with custom date format', async () => {
//     await wrapper.setProps({ dateFormat: 'd.m.Y' });
//     expect(wrapper.html()).toContain('dateformat="d.m.Y"');
//   });
//
//   test('Renders datetime picker with custom start date', async () => {
//     let customStartDate = new Date();
//     await wrapper.setProps({ startDate: customStartDate });
//     expect(wrapper.html()).toContain('startdate="' + customStartDate + '"');
//   });
//
//   test('Renders datetime picker with custom step minutes', async () => {
//     await wrapper.setProps({ stepMinutes: 30 });
//     expect(wrapper.html()).toContain('stepminutes="30"');
//   });
//
//   test('Renders inline datetime picker', async () => {
//     await wrapper.setProps({ inlinePicker: true });
//     expect(wrapper.html()).toContain('inlinepicker="true"');
//   });
//
//   test('Renders datetime picker with custom locale', async () => {
//     await wrapper.setProps({ locale: 'de' });
//     expect(wrapper.html()).toContain('locale="de"');
//   });
// });
