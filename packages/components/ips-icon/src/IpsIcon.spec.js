import { shallowMount } from '@vue/test-utils';
import IpsIcon from './IpsIcon.vue';

let wrapper;

describe('IpsIcon.vue', () => {
  beforeEach(function () {
    wrapper = shallowMount(IpsIcon, {
      propsData: {
        icon: 'ico-star',
      },
    });
  });

  test('renders a i element', () => {
    expect(wrapper.find('i').exists()).toBe(true);
  });

  test('Renders icon with barcode icon per default with all dots', () => {
    expect(wrapper.html()).toContain('<i class="ips ips-icon ico-star ips-icon-md"></i>');
  });

  describe('size prop validation', () => {
    let validator;

    beforeEach(() => {
      validator = IpsIcon.props.size.validator;
    });

    test('should return false if not valid value', () => {
      expect(validator('test')).toBeFalsy();
    });

    test('should return true if a valid value', () => {
      expect(validator('lg')).toBeTruthy();
    });
  });

  describe('Computed', () => {
    describe('iconClasses()', () => {
      test('should return an array', () => {
        expect(typeof wrapper.vm.iconClasses).toBe('object');
      });

      test('should include "ips-icon-md" when size prop is not provided', () => {
        const expected = ['ips-icon-md'];
        expect(wrapper.vm.iconClasses).toEqual(expect.arrayContaining(expected));
      });

      test('should include "ips-icon-lg" when size prop is "sm"', async () => {
        await wrapper.setProps({ size: 'sm' });
        const expected = ['ips-icon-sm'];
        expect(wrapper.vm.iconClasses).toEqual(expect.arrayContaining(expected));
      });

      test('should include "ips-icon-x2" when size prop is "md"', () => {
        wrapper.setProps({ size: 'md' });
        const expected = ['ips-icon-md'];
        expect(wrapper.vm.iconClasses).toEqual(expect.arrayContaining(expected));
      });

      test('should include "ips-icon-x3" when size prop is "lg"', async () => {
        await wrapper.setProps({ size: 'lg' });
        const expected = ['ips-icon-lg'];
        expect(wrapper.vm.iconClasses).toEqual(expect.arrayContaining(expected));
      });
    });
  });

  test('Emits icon click event', () => {
    wrapper.find('i').trigger('click');
  });
});
