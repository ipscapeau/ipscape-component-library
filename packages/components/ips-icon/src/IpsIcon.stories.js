import IpsIcon from '../src/IpsIcon.vue';
import { icons } from './icons';
import IpsTooltip from '../../ips-tooltip/src/IpsTooltip';

export default {
  title: 'Components/IpsIcon',
  component: IpsIcon,
  argTypes: {
    size: {
      control: {
        type: 'select',
        options: ['sm', 'md', 'lg'],
      },
    },
    icon: {
      control: {
        type: 'select',
        options: icons,
      },
    },
  },
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsIcon },
  template: '<ips-icon :icon="icon" :size="size"></ips-icon>',
});

export const basic = template.bind({});
basic.args = {};

const template2 = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsIcon, IpsTooltip },
  template:
    '<ips-tooltip :tooltip="tooltip" :entry-delay="entryDelay" :exit-delay="exitDelay">' +
    '<ips-icon :icon="icon" :size="size"></ips-icon>' +
    '</ips-tooltip>',
});

export const tooltip = template2.bind({});
tooltip.args = {
  tooltip: 'This is the tooltip text',
  entryDelay: 100,
  exitDelay: 100,
  icon: icons[21],
  size: 'lg',
};
