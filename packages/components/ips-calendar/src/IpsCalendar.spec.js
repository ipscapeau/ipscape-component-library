import { mount } from '@vue/test-utils';
import IpsCalendar from './IpsCalendar.vue';

let wrapper;

describe('IpsCalendar.vue', () => {
  beforeEach(() => {
    wrapper = mount(IpsCalendar, {});
  });

  test('Renders calendar', () => {
    expect(wrapper.find('div.vc-container').exists()).toBe(true);
  });
});
