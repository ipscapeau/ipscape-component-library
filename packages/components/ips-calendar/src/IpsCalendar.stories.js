import IpsCalendar from '../src/IpsCalendar.vue';

export default {
  title: 'Components/IpsCalendar',
  component: IpsCalendar,
  argTypes: {},
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsCalendar },
  template: `<ips-calendar></ips-calendar>`,
});

export const basic = template.bind({});
basic.args = {};
