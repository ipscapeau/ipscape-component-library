import IpsFilePicker from '../src/IpsFilePicker.vue';

export default {
  title: 'Components/IpsFilePicker',
  component: IpsFilePicker,
  argTypes: {
    sizing: {
      control: {
        type: 'select',
        options: ['medium', 'small', 'large'],
      },
    },
  },
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsFilePicker },
  template:
    '<ips-file-picker :id="id" :value="value" :classes="classes" :label-classes="labelClasses" :sizing="sizing" :max-height="maxHeight" :label="label" :display-label="displayLabel" :clear-on-focus="clearOnFocus" :placeholder="placeholder" :disabled="disabled" :required="required" :error="error" :tooltip="tooltip" :icon="icon" :iconAppend="iconAppend" :plainText="plainText" :clearable="clearable"></ips-file-picker>',
});

export const basic = template.bind({});
basic.args = {
  id: 'basic',
  label: 'Basic label',
};
