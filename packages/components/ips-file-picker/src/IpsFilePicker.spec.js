import { shallowMount } from '@vue/test-utils';
import IpsFilePicker from './IpsFilePicker.vue';

let wrapper;

describe('IpsFilePicker', () => {
  beforeEach(() => {
    wrapper = shallowMount(IpsFilePicker, {
      propsData: {
        id: 'filepickerId',
        label: 'Test Label',
      },
    });
  });

  test('Renders default file picker', () => {
    const inputHtml = wrapper.find('input').html();
    expect(inputHtml).toContain('<input type="file" class="file">');
  });

  test('should render a tooltip', async () => {
    await wrapper.setProps({ tooltip: 'Test tooltip' });
    const tip = wrapper.find('span[data-bs-toggle="tooltip"]');
    expect(tip).toBeDefined();
  });

  describe('filepicker field label', () => {
    test('should render and display a label', async () => {
      await wrapper.setProps({ label: 'this is a label', displayLabel: true });
      expect(wrapper.html()).toContain('label');
      expect(wrapper.html()).not.toContain('visually-hidden');
    });
    test('should render a label for screenreaders', async () => {
      expect(wrapper.html()).toContain('label');
      expect(wrapper.html()).toContain('visually-hidden');
    });
  });

  test('Renders multiple file picker', async () => {
    await wrapper.setProps({ multiple: true });
    expect(wrapper.html()).toContain(
      '<input id="filepickerId" type="text" class="form-control ips" multiple="multiple">'
    );
  });

  test('Emits change event for single file selection', () => {
    wrapper.find('input').trigger('change');
  });

  test('Emits change event for mutliple file selection', async () => {
    await wrapper.setProps({ multiple: true });
    wrapper.find('input').trigger('change');
  });
});
