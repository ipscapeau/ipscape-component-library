import IpsCountdown from '../src/IpsCountdown.vue';

export default {
  title: 'Components/IpsCountdown',
  component: IpsCountdown,
  argTypes: {},
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsCountdown },
  template:
    '<ips-countdown :classes="classes" :initial-counter="initialCounter" :size="size" :stroke="stroke" @timeout="doSomething"></ips-countdown>',
  methods: {
    doSomething() {
      alert('Timeout complete');
    },
  },
});

export const basic = template.bind({});
basic.args = {
  initialCounter: 55,
};
