import { shallowMount } from '@vue/test-utils';
import IpsCountdown from './IpsCountdown.vue';

let wrapper;

describe('IpsACountdown.vue', () => {
  beforeEach(() => {
    wrapper = shallowMount(IpsCountdown, {
      propsData: {
        classes: 'alert-primary',
        initialCounter: 20,
        size: 50,
        stroke: 3,
      },
    });
  });

  test('renders a countdown timer', () => {
    expect(wrapper.find('svg').exists()).toBe(true);
  });
});
