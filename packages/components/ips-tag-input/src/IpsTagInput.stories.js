import IpsTagInput from '../src/IpsTagInput.vue';

export default {
  title: 'Components/IpsTagInput',
  component: IpsTagInput,
  argTypes: {},
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsTagInput },
  template:
    '<ips-tag-input :id="id" :label="label" :placeholder="placeholder" :tags="tags" :display-label="displayLabel" :classes="classes" :label-classes="labelClasses" :tooltip="tooltip" :required="required" :readonly="readonly" :disabled="disabled"></ips-tag-input>',
});

export const basic = template.bind({});
basic.args = {
  id: 'tagInputId',
  label: 'Tag input label',
  placeholder: 'Enter an email address',
  tags: [
    {
      value: 'olivier.liechti@ipscape.com',
      variant: 'gray',
      variantStyle: 'subtle',
    },
    {
      value: 'invalid input',
      variant: 'red',
      variantStyle: 'subtle',
    },
  ],
};
