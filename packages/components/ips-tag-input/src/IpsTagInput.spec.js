import { mount } from '@vue/test-utils';
import IpsTagInput from './IpsTagInput.vue';

let wrapper;

describe('IpsTagInput.vue', () => {
  beforeEach(function () {
    wrapper = mount(IpsTagInput, {
      propsData: {
        id: 'tagInputId',
        label: 'tag input label',
        tags: [
          {
            value: 'value1',
            variant: 'gray',
            variantStyle: 'subtle',
            removable: true,
          },
        ],
      },
    });
  });

  test('Renders default tag input component with one tag', () => {
    expect(wrapper.find('.tag-input').exists()).toBe(true);
    expect(wrapper.find('.tag-input-tag').exists()).toBe(true);
    expect(wrapper.find('.tag-input-tag').html()).toContain('class="badge badge-gray-subtle');
  });

  test('Removes tag when using backspace', async () => {
    const removeTagSpy = jest.spyOn(wrapper.vm, 'removeTag');
    const removeLastTagSpy = jest.spyOn(wrapper.vm, 'removeLastTag');
    const updatedTagsSpy = jest.spyOn(wrapper.vm, 'updatedTags');
    expect(wrapper.find('.tag-input-tag').exists()).toBe(true);
    await wrapper.find('#tagInputId').trigger('keydown.backspace');
    expect(removeTagSpy).toHaveBeenCalled();
    expect(removeLastTagSpy).toHaveBeenCalled();
    expect(updatedTagsSpy).toHaveBeenCalled();
    expect(wrapper.find('.tag-input-tag').exists()).toBe(false);
  });

  test('Calls add tag method when pressing enter', async () => {
    const addTagSpy = jest.spyOn(wrapper.vm, 'addTag');
    const updatedTagsSpy = jest.spyOn(wrapper.vm, 'updatedTags');
    expect(wrapper.findAll('.tag-input-tag').length).toBe(1);
    const input = wrapper.find('#tagInputId');
    input.element.value = 'new tag';
    input.trigger('input');
    await input.trigger('keydown.enter');
    expect(addTagSpy).toHaveBeenCalled();
    expect(updatedTagsSpy).toHaveBeenCalled();
    expect(wrapper.findAll('.tag-input-tag').length).toBe(2);
  });

  test('Calls add tag method when pressing comma', async () => {
    const addTagSpy = jest.spyOn(wrapper.vm, 'addTag');
    const updatedTagsSpy = jest.spyOn(wrapper.vm, 'updatedTags');
    expect(wrapper.findAll('.tag-input-tag').length).toBe(1);
    const input = wrapper.find('#tagInputId');
    input.element.value = 'new tag';
    input.trigger('input');
    await wrapper.find('#tagInputId').trigger('keydown', {
      keyCode: 188,
    });
    expect(addTagSpy).toHaveBeenCalled();
    expect(updatedTagsSpy).toHaveBeenCalled();
    expect(wrapper.findAll('.tag-input-tag').length).toBe(2);
  });

  test('should render a tooltip', async () => {
    await wrapper.setProps({ tooltip: 'Test tooltip' });
    const tip = wrapper.find('span[data-bs-toggle="tooltip"]');
    expect(tip).toBeDefined();
  });
});
