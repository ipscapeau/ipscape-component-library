# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.19](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/ips-tag-input@1.0.19-develop.0...@ipscape/ips-tag-input@1.0.19) (2024-05-14)

**Note:** Version bump only for package @ipscape/ips-tag-input

## [1.0.18](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/ips-tag-input@1.0.18-develop.0...@ipscape/ips-tag-input@1.0.18) (2024-04-15)

**Note:** Version bump only for package @ipscape/ips-tag-input

## [1.0.17](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/ips-tag-input@1.0.17-develop.4...@ipscape/ips-tag-input@1.0.17) (2024-01-29)

**Note:** Version bump only for package @ipscape/ips-tag-input

## [1.0.16](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/ips-tag-input@1.0.16-develop.0...@ipscape/ips-tag-input@1.0.16) (2023-11-02)

**Note:** Version bump only for package @ipscape/ips-tag-input

## [1.0.15](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/ips-tag-input@1.0.15-develop.1...@ipscape/ips-tag-input@1.0.15) (2023-09-21)

**Note:** Version bump only for package @ipscape/ips-tag-input

## [1.0.14](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/ips-tag-input@1.0.14-develop.1...@ipscape/ips-tag-input@1.0.14) (2023-05-22)

**Note:** Version bump only for package @ipscape/ips-tag-input

## [1.0.13](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/ips-tag-input@1.0.13-develop.0...@ipscape/ips-tag-input@1.0.13) (2023-04-18)

**Note:** Version bump only for package @ipscape/ips-tag-input

## [1.0.12](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/ips-tag-input@1.0.12-develop.4...@ipscape/ips-tag-input@1.0.12) (2023-02-16)

**Note:** Version bump only for package @ipscape/ips-tag-input

## [1.0.11](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/ips-tag-input@1.0.11-develop.0...@ipscape/ips-tag-input@1.0.11) (2022-11-15)

**Note:** Version bump only for package @ipscape/ips-tag-input

## [1.0.10](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/ips-tag-input@1.0.10-develop.2...@ipscape/ips-tag-input@1.0.10) (2022-10-17)

**Note:** Version bump only for package @ipscape/ips-tag-input

## [1.0.9](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/ips-tag-input@1.0.9-develop.0...@ipscape/ips-tag-input@1.0.9) (2022-08-24)

**Note:** Version bump only for package @ipscape/ips-tag-input

## [1.0.8](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/ips-tag-input@1.0.8-develop.0...@ipscape/ips-tag-input@1.0.8) (2022-08-22)

**Note:** Version bump only for package @ipscape/ips-tag-input

## [1.0.7](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/ips-tag-input@1.0.7-develop.0...@ipscape/ips-tag-input@1.0.7) (2022-08-17)

**Note:** Version bump only for package @ipscape/ips-tag-input

## [1.0.6](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/ips-tag-input@1.0.6-develop.1...@ipscape/ips-tag-input@1.0.6) (2022-07-21)

**Note:** Version bump only for package @ipscape/ips-tag-input

## [1.0.5](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/ips-tag-input@1.0.5-develop.0...@ipscape/ips-tag-input@1.0.5) (2022-07-04)

**Note:** Version bump only for package @ipscape/ips-tag-input

## [1.0.4](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/ips-tag-input@1.0.4-develop.0...@ipscape/ips-tag-input@1.0.4) (2022-01-20)

**Note:** Version bump only for package @ipscape/ips-tag-input

## [1.0.3](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/ips-tag-input@1.0.2...@ipscape/ips-tag-input@1.0.3) (2021-10-11)

**Note:** Version bump only for package @ipscape/ips-tag-input

## [1.0.2](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/ips-tag-input@1.0.2-develop.4...@ipscape/ips-tag-input@1.0.2) (2021-06-10)

**Note:** Version bump only for package @ipscape/ips-tag-input

## [1.0.1](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/ips-tag-input@1.0.1-develop.0...@ipscape/ips-tag-input@1.0.1) (2021-03-18)

**Note:** Version bump only for package @ipscape/ips-tag-input
