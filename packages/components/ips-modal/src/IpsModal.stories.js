import IpsModal from '../src/IpsModal.vue';
import IpsAlert from '../../ips-alert/src/IpsAlert.vue';
import IpsButton from '../../ips-button/src/IpsButton.vue';
import IpsIcon from '../../ips-icon/src/IpsIcon.vue';

export default {
  title: 'Components/IpsModal',
  component: IpsModal,
  argTypes: {},
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsModal, IpsButton },
  template: `<div>
    <ips-button @click="showModal">Show Modal</ips-button>
    <ips-modal :modal-title-id="modalTitleId" :is-hidden="isHidden" v-show="isModalVisible" @close="closeModal">
      <span slot="header">
        <h1 id="exampleTitle">This is the modal title</h1>
      </span>
      <span slot="body">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</span>
      <span slot="footer_right">
        <IpsButton @click="closeModal">Close</IpsButton>
      </span>
    </ips-modal>
  </div>`,
  data: () => ({ isModalVisible: false }),
  methods: {
    showModal() {
      this.isModalVisible = true;
    },
    closeModal() {
      this.isModalVisible = false;
    },
  },
});

const templateAlert = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsModal, IpsAlert, IpsButton },
  template: `<div>
    <ips-button @click="showModal">Show Modal</ips-button>
    <ips-modal v-show="isModalVisible" @close="closeModal" modalTitle="exampleTitle" :alertType="alertType" :isHidden="!isModalVisible">
      <span slot="header">
        <h1 id="exampleTitle">This is the modal title</h1>
      </span>
      <span slot="alerts">
        <ips-alert
          :variant="alertType"
          title="This is an alert"
          message="This is an alert message">
        </ips-alert>
      </span>
      <span slot="body">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</span>
      <span slot="footer_right">
        <ips-button @click="closeModal">Close</ips-button>
      </span>
    </ips-modal>
  </div>`,
  data: () => ({ isModalVisible: false }),
  methods: {
    showModal() {
      this.isModalVisible = true;
    },
    closeModal() {
      this.isModalVisible = false;
    },
  },
});

const templateCustom = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsModal, IpsButton, IpsIcon },
  template: `<div>
    <ips-button @click="showModal">Show Modal</ips-button>
    <ips-modal v-show="isModalVisible" @close="closeModal" modalTitle="exampleTitle" :isHidden="!isModalVisible">
      <header class="modal-header">
        <h1 id="exampleTitle">Heading Text</h1>
        <ips-icon icon="ico-close" @click="closeModal" style="cursor: pointer"></ips-icon>
      </header>
      <section class="modal-body">
        This example illustrates the use of the modal without nominated slots
      </section>
      <footer class="modal-footer justify-content-between">
        <div>
          <span>Help Text</span>
        </div>
        <div>
          <ips-button @click="closeModal">Close</ips-button>
        </div>
      </footer>
    </ips-modal>
  </div>`,
  data: () => ({ isModalVisible: false }),
  methods: {
    showModal() {
      this.isModalVisible = true;
    },
    closeModal() {
      this.isModalVisible = false;
    },
  },
});

export const basic = template.bind({});
basic.args = {
  modalTitleId: 'basic',
};

export const alert = templateAlert.bind({});
alert.args = {
  modalTitleId: 'alert',
  alertType: 'danger',
};

export const custom = templateCustom.bind({});
custom.args = {
  modalTitleId: 'custom',
};
