import { mount } from '@vue/test-utils';
import IpsModal from './IpsModal.vue';

let wrapper;

describe('IpsModal.vue', () => {
  beforeEach(function () {
    wrapper = mount(IpsModal);
  });

  test('Renders default modal', () => {
    const backdrop = wrapper.find('div.modal-backdrop');
    expect(backdrop.find('div.modal').exists()).toBe(true);
  });

  test('Emits close icon click event', () => {
    wrapper.find('i').trigger('click');
  });
});
