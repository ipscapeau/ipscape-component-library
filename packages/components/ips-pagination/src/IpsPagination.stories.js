import IpsPagination from '../src/IpsPagination.vue';

export default {
  title: 'Components/IpsPagination',
  component: IpsPagination,
  argTypes: {},
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsPagination },
  template:
    '<ips-pagination :id="id" :total-items="totalItems" :selected-page="selectedPage" :max-pages-visible="maxPagesVisible" @pagechanged="doSomething"></ips-pagination>',
  methods: {
    doSomething(page) {
      console.info('selected page ', page);
    },
  },
});

export const basic = template.bind({});
basic.args = {
  id: 'basic',
};
