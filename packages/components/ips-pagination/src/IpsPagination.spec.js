import { mount } from '@vue/test-utils';
import IpsPagination from './IpsPagination.vue';

describe('IpsPagination.vue', () => {
  let wrapper;

  beforeEach(function () {
    wrapper = mount(IpsPagination, {
      propsData: { totalItems: 0 },
    });
  });

  test('Renders default pagination', async () => {
    expect(wrapper.html()).toContain('<nav aria-label="Page navigation">');
    expect(wrapper.html()).toContain('<ul class="pagination">');
  });

  test('Renders pagination with one page', async () => {
    await wrapper.setProps({ totalItems: 11 });
    expect(wrapper.html()).toContain('<nav aria-label="Page navigation">');
    expect(wrapper.html()).toContain(
      '<li class="page-item active"><a id="showPage1" aria-label="Go to page 1" class="page-link">'
    );
    expect(wrapper.html()).not.toContain(
      '<a id="showPage2" aria-label="Go to page 2" class="page-link">'
    );
  });

  test('Renders pagination with 5 pages', async () => {
    await wrapper.setProps({ totalItems: 101 });
    expect(wrapper.html()).toContain(
      '<a id="showFirstPage" aria-label="Go to first page" class="page-link">'
    );
    expect(wrapper.html()).toContain(
      '<a id="showPage2" aria-label="Go to page 2" class="page-link">'
    );
    expect(wrapper.html()).toContain(
      '<a id="showPage3" aria-label="Go to page 3" class="page-link">'
    );
    expect(wrapper.html()).toContain(
      '<a id="showPage4" aria-label="Go to page 4" class="page-link">'
    );
    expect(wrapper.html()).toContain(
      '<a id="showPage1" aria-label="Go to page 1" class="page-link">'
    );
  });

  test('Renders pagination with 5 pages only', async () => {
    await wrapper.setProps({ totalItems: 6 });
    expect(wrapper.html()).not.toContain(
      '<a id="showPage6" aria-label="Go to page 6" class="page-link">'
    );
  });

  test('Emits click event for showFirstPage', async () => {
    await wrapper.setProps({ totalItems: 101, selectedPage: 2 });
    await wrapper.find('#showFirstPage').trigger('click');
  });

  test('Emits click event for showPreviousPage', async () => {
    await wrapper.setProps({ totalItems: 6, selectedPage: 2 });
    await wrapper.find('#showPreviousPage').trigger('click');
  });

  test('Emits click event for showNextPage', async () => {
    await wrapper.setProps({ totalItems: 6, selectedPage: 2 });
    await wrapper.find('#showNextPage').trigger('click');
  });

  test('Emits click event for showLastPage', async () => {
    await wrapper.setProps({ totalItems: 101, selectedPage: 2 });
    await wrapper.find('#showLastPage').trigger('click');
  });

  test('Emits click event for showLastPage3', async () => {
    await wrapper.setProps({ totalItems: 100, selectedPage: 2 });
    await wrapper.find('#showPage3').trigger('click');
  });

  test('Emits click event on per page select', async () => {
    await wrapper.setProps({ totalItems: 100, selectedPage: 2 });
    await wrapper.find('select').trigger('change');
  });

  test('Emits click event for showPage6 to test page shift', async () => {
    await wrapper.setProps({ totalItems: 200 });
    await wrapper.find('#showPage5').trigger('click');
    await wrapper.find('#showPage6').trigger('click');
  });
});
