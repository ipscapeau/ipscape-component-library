import IpsLink from '../src/IpsLink.vue';

export default {
  title: 'Components/IpsLink',
  component: IpsLink,
  argTypes: {
    linkText: {
      control: {
        type: 'text',
      },
      defaultValue: 'Link text',
    },
    target: {
      control: {
        type: 'select',
        options: ['_blank', '_self', '_parent', '_top'],
      },
      defaultValue: '_self',
    },
  },
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsLink },
  template: '<ips-link :href="href" :target="target">{{ linkText }}</ips-link>',
});

export const basic = template.bind({});
basic.args = {};
