import { shallowMount } from '@vue/test-utils';
import IpsLink from './IpsLink.vue';

describe('IpsLink.vue', () => {
  let wrapper;

  beforeEach(function () {
    wrapper = shallowMount(IpsLink, {
      propsData: {},
    });
  });

  test('Renders default link', () => {
    expect(wrapper.html()).toContain('<a href="" target="_self" class="btn-link"></a>');
  });

  test('Renders link with url', async () => {
    const href = 'http://www.ipscape.com';
    await wrapper.setProps({ href });
    expect(wrapper.html()).toContain(
      '<a href="http://www.ipscape.com" target="_self" class="btn-link"></a>'
    );
  });

  test('Renders link with url and target blank', async () => {
    const href = 'http://www.ipscape.com';
    const target = '_blank';
    await wrapper.setProps({ href, target });
    expect(wrapper.html()).toContain(
      '<a href="http://www.ipscape.com" target="_blank" class="btn-link"></a>'
    );
  });
});
