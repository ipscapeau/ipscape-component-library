import IpsAlert from '../src/IpsAlert.vue';

export default {
  title: 'Components/IpsAlert',
  component: IpsAlert,
  argTypes: {
    variant: {
      control: {
        type: 'select',
        options: ['danger', 'info', 'message', 'success', 'warning'],
      },
    },
  },
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsAlert },
  template: '<ips-alert :variant="variant" :title="title" :message="message"></ips-alert>',
});

export const basic = template.bind({});
basic.args = {
  message: 'The alert message',
  title: 'The alert title',
  variant: 'success',
};
