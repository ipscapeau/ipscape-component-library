import { shallowMount } from '@vue/test-utils';
import IpsAlert from './IpsAlert.vue';

let wrapper;

describe('IpsAlert.vue', () => {
  beforeEach(() => {
    wrapper = shallowMount(IpsAlert, {
      propsData: {
        title: 'Alert title',
        message: 'Alert message',
      },
    });
  });

  test('alert heading contains title', () => {
    expect(wrapper.find('.alert-heading').exists()).toBe(true);
    const body = wrapper.find('.alert-heading');
    expect(body.html()).toContain('Alert title');
  });

  test('alert body contains message', async () => {
    expect(wrapper.find('.alert-body').exists()).toBe(true);
    const body = wrapper.find('.alert-body');
    expect(body.html()).toContain('Alert message');
  });

  describe('Computed props', () => {
    describe('computedStyleClasses()', () => {
      test('should return an array', () => {
        expect(typeof wrapper.vm.computedStyleClasses).toBe('object');
      });

      test('should return "alert-secondary" if prop not set', async () => {
        const expected = ['alert-secondary'];
        expect(wrapper.vm.computedStyleClasses).toEqual(expect.arrayContaining(expected));
      });

      test('should return "alert-secondary" if prop set', async () => {
        await wrapper.setProps({ variant: 'message' });
        const expected = ['alert-secondary'];
        expect(wrapper.vm.computedStyleClasses).toEqual(expect.arrayContaining(expected));
      });

      test('should return "alert-danger" if prop set', async () => {
        await wrapper.setProps({ variant: 'danger' });
        const expected = ['alert-danger'];
        expect(wrapper.vm.computedStyleClasses).toEqual(expect.arrayContaining(expected));
      });

      test('should return "alert-info" if classes prop is set', async () => {
        await wrapper.setProps({ variant: 'info' });
        const expected = ['alert-info'];
        expect(wrapper.vm.computedStyleClasses).toEqual(expect.arrayContaining(expected));
      });

      test('should return "alert-success" if classes prop is set', async () => {
        await wrapper.setProps({ variant: 'success' });
        const expected = ['alert-success'];
        expect(wrapper.vm.computedStyleClasses).toEqual(expect.arrayContaining(expected));
      });

      test('should return "alert-warning" if classes prop is set', async () => {
        await wrapper.setProps({ variant: 'warning' });
        const expected = ['alert-warning'];
        expect(wrapper.vm.computedStyleClasses).toEqual(expect.arrayContaining(expected));
      });
    });
  });
});
