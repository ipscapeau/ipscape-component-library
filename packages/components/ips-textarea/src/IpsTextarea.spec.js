import { shallowMount } from '@vue/test-utils';
import IpsTextarea from './IpsTextarea.vue';

let wrapper;

describe('IpsTextarea.vue', () => {
  beforeEach(function () {
    wrapper = shallowMount(IpsTextarea, {
      propsData: {
        id: 'default_id',
        label: 'Test Label',
      },
    });
  });

  test('Renders default textarea', () => {
    expect(wrapper.find('textarea').exists()).toBe(true);
    expect(wrapper.find('label').exists()).toBe(true);
  });

  describe('textarea label', () => {
    test('should render and display a label', async () => {
      await wrapper.setProps({ label: 'this is a label', displayLabel: true });
      expect(wrapper.html()).toContain('label');
      expect(wrapper.html()).not.toContain('visually-hidden');
    });
    test('should render a label for screenreaders', async () => {
      expect(wrapper.html()).toContain('label');
      expect(wrapper.html()).toContain('visually-hidden');
    });
  });

  test('Renders small textarea', async () => {
    await wrapper.setProps({ sizing: 'small' });
    const expected = ['form-control-sm'];
    expect(wrapper.vm.inputComputedClasses).toEqual(expect.arrayContaining(expected));
  });

  test('Renders large textarea', async () => {
    await wrapper.setProps({ sizing: 'large' });
    const expected = ['form-control-lg'];
    expect(wrapper.vm.inputComputedClasses).toEqual(expect.arrayContaining(expected));
  });

  test('should render a placeholder if given prop', async () => {
    await wrapper.setProps({ placeholder: 'Foo' });
    const inputHtml = wrapper.find('textarea').html();
    expect(inputHtml).toContain('placeholder="Foo"');
  });

  test('should render a tooltip', async () => {
    await wrapper.setProps({ tooltip: 'Test tooltip' });
    const tip = wrapper.find('span[data-bs-toggle="tooltip"]');
    expect(tip).toBeDefined();
  });

  test('Emits button click event', () => {
    wrapper.find('textarea').trigger('input');
  });
});
