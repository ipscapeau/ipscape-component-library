import IpsTextarea from '../src/IpsTextarea.vue';

export default {
  title: 'Components/IpsTextarea',
  component: IpsTextarea,
  argTypes: {
    sizing: {
      control: {
        type: 'select',
        options: ['small', 'medium', 'large'],
      },
    },
  },
};

const template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { IpsTextarea },
  template:
    '<ips-textarea :id="id" :value="value" :classes="classes" :label-classes="labelClasses" :sizing="sizing" :max-height="maxHeight" :label="label" :display-label="displayLabel" :clear-on-focus="clearOnFocus" :placeholder="placeholder" :disabled="disabled" :required="required" :error="error" :tooltip="tooltip" :icon="icon" :iconAppend="iconAppend" :plainText="plainText" :clearable="clearable"></ips-textarea>',
});

export const basic = template.bind({});
basic.args = {
  id: 'basic',
  label: 'Basic label',
};
