import IpsAlert from '../components/ips-alert';
import IpsAvatar from '../components/ips-avatar';
import IpsBadge from '../components/ips-badge';
import IpsButton from '../components/ips-button';
import IpsCalendar from '../components/ips-calendar';
import IpsCheckbox from '../components/ips-checkbox';
import IpsCountdown from '../components/ips-countdown';
import IpsDropdown from '../components/ips-dropdown';
import IpsDatePicker from '../components/ips-date-picker';
import IpsEditor from '../components/ips-editor';
import IpsFilePicker from '../components/ips-file-picker';
import IpsIcon from '../components/ips-icon';
import IpsInput from '../components/ips-input';
import IpsLink from '../components/ips-link';
import IpsModal from '../components/ips-modal';
import IpsPagination from '../components/ips-pagination';
import IpsSelect from '../components/ips-select';
import IpsSidebar from '../components/ips-sidebar';
import IpsSpinner from '../components/ips-spinner';
import IpsTagInput from '../components/ips-tag-input';
import IpsTelInput from '../components/ips-tel-input';
import IpsTextarea from '../components/ips-textarea';
import IpsTable from '../components/ips-table';
import IpsTheme from '../components/ips-theme';
import IpsToast from '../components/ips-toast';
import IpsTooltip from '../components/ips-tooltip';
import tooltip from '../directives/tooltip';

export default {
  install(Vue) {
    Vue.component(IpsAlert.name, IpsAlert);
    Vue.component(IpsAvatar.name, IpsAvatar);
    Vue.component(IpsBadge.name, IpsBadge);
    Vue.component(IpsButton.name, IpsButton);
    Vue.component(IpsCalendar.name, IpsCalendar);
    Vue.component(IpsCheckbox.name, IpsCheckbox);
    Vue.component(IpsCountdown.name, IpsCountdown);
    Vue.component(IpsDatePicker.name, IpsDatePicker);
    Vue.component(IpsDropdown.name, IpsDropdown);
    Vue.component(IpsEditor.name, IpsEditor);
    Vue.component(IpsFilePicker.name, IpsFilePicker);
    Vue.component(IpsIcon.name, IpsIcon);
    Vue.component(IpsInput.name, IpsInput);
    Vue.component(IpsLink.name, IpsLink);
    Vue.component(IpsModal.name, IpsModal);
    Vue.component(IpsPagination.name, IpsPagination);
    Vue.component(IpsSelect.name, IpsSelect);
    Vue.component(IpsSidebar.name, IpsSidebar);
    Vue.component(IpsSpinner.name, IpsSpinner);
    Vue.component(IpsTagInput.name, IpsTagInput);
    Vue.component(IpsTelInput.name, IpsTelInput);
    Vue.component(IpsTextarea.name, IpsTextarea);
    Vue.component(IpsTable.name, IpsTable);
    Vue.component(IpsTheme.name, IpsTheme);
    Vue.component(IpsToast.name, IpsToast);
    Vue.component(IpsTooltip.name, IpsTooltip);
    Vue.directive('tooltip', tooltip);
  },
};

export {
  IpsAlert,
  IpsAvatar,
  IpsBadge,
  IpsButton,
  IpsCalendar,
  IpsCheckbox,
  IpsCountdown,
  IpsDatePicker,
  IpsDropdown,
  IpsEditor,
  IpsFilePicker,
  IpsIcon,
  IpsInput,
  IpsLink,
  IpsModal,
  IpsPagination,
  IpsSelect,
  IpsSidebar,
  IpsSpinner,
  IpsTagInput,
  IpsTelInput,
  IpsTextarea,
  IpsTable,
  IpsTheme,
  IpsToast,
  IpsTooltip,
  tooltip,
};
