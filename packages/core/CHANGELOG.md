# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.35](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.35-develop.4...@ipscape/cumulus@1.0.35) (2024-05-14)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.34](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.34-develop.0...@ipscape/cumulus@1.0.34) (2024-04-15)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.33](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.33-develop.2...@ipscape/cumulus@1.0.33) (2024-03-19)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.32](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.32-develop.11...@ipscape/cumulus@1.0.32) (2024-01-29)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.31](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.31-develop.4...@ipscape/cumulus@1.0.31) (2023-11-02)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.30](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.30-develop.9...@ipscape/cumulus@1.0.30) (2023-09-21)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.29](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.29-develop.4...@ipscape/cumulus@1.0.29) (2023-08-07)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.28](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.28-develop.1...@ipscape/cumulus@1.0.28) (2023-07-12)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.27](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.27-develop.0...@ipscape/cumulus@1.0.27) (2023-06-08)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.26](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.26-develop.2...@ipscape/cumulus@1.0.26) (2023-05-22)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.25](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.25-develop.3...@ipscape/cumulus@1.0.25) (2023-04-18)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.24](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.24-develop.16...@ipscape/cumulus@1.0.24) (2023-02-16)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.23](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.23-develop.6...@ipscape/cumulus@1.0.23) (2022-11-15)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.22](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.22-develop.6...@ipscape/cumulus@1.0.22) (2022-10-17)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.21](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.21-develop.0...@ipscape/cumulus@1.0.21) (2022-09-21)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.20](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.20-develop.0...@ipscape/cumulus@1.0.20) (2022-08-24)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.19](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.19-develop.0...@ipscape/cumulus@1.0.19) (2022-08-22)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.18](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.18-develop.3...@ipscape/cumulus@1.0.18) (2022-08-17)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.17](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.17-develop.2...@ipscape/cumulus@1.0.17) (2022-07-21)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.16](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.16-develop.4...@ipscape/cumulus@1.0.16) (2022-07-04)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.15](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.15-develop.0...@ipscape/cumulus@1.0.15) (2022-05-02)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.14](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.14-develop.5...@ipscape/cumulus@1.0.14) (2022-04-28)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.13](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.13-develop.0...@ipscape/cumulus@1.0.13) (2022-02-03)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.12](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.12-develop.0...@ipscape/cumulus@1.0.12) (2022-01-20)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.11](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.11-develop.15...@ipscape/cumulus@1.0.11) (2022-01-20)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.10](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.10-develop.29...@ipscape/cumulus@1.0.10) (2021-10-11)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.9](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.9-develop.16...@ipscape/cumulus@1.0.9) (2021-08-12)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.8](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.8-develop.32...@ipscape/cumulus@1.0.8) (2021-06-10)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.7](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.7-develop.2...@ipscape/cumulus@1.0.7) (2021-03-18)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.5](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.4...@ipscape/cumulus@1.0.5) (2021-02-18)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.4](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.3...@ipscape/cumulus@1.0.4) (2021-02-17)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.3](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.2...@ipscape/cumulus@1.0.3) (2021-02-16)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.2](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.1...@ipscape/cumulus@1.0.2) (2021-02-08)

**Note:** Version bump only for package @ipscape/cumulus

## [1.0.1](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.1-develop.0...@ipscape/cumulus@1.0.1) (2021-02-02)

**Note:** Version bump only for package @ipscape/cumulus

# [1.0.0](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@1.0.0-develop.2...@ipscape/cumulus@1.0.0) (2021-02-02)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.145](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.145-develop.1...@ipscape/cumulus@0.0.145) (2021-01-27)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.144](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.144-develop.1...@ipscape/cumulus@0.0.144) (2021-01-27)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.139](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.138...@ipscape/cumulus@0.0.139) (2020-11-05)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.127](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.126...@ipscape/cumulus@0.0.127) (2020-07-28)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.124](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.124-develop.0...@ipscape/cumulus@0.0.124) (2020-07-13)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.123](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.123-CL-20-remove-click-stop-on-icon.0...@ipscape/cumulus@0.0.123) (2020-07-07)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.118](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.118-develop.3...@ipscape/cumulus@0.0.118) (2020-06-24)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.117](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.117-develop.0...@ipscape/cumulus@0.0.117) (2020-05-15)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.115](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.115-develop.0...@ipscape/cumulus@0.0.115) (2020-05-15)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.114](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.113...@ipscape/cumulus@0.0.114) (2020-05-15)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.110](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.109...@ipscape/cumulus@0.0.110) (2020-05-12)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.101](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.101-develop.1...@ipscape/cumulus@0.0.101) (2020-04-06)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.94](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.93...@ipscape/cumulus@0.0.94) (2020-03-13)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.92](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.91...@ipscape/cumulus@0.0.92) (2020-03-12)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.88](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.88-develop.0...@ipscape/cumulus@0.0.88) (2020-02-26)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.87](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.87-develop.1...@ipscape/cumulus@0.0.87) (2020-01-27)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.86](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.86-develop.0...@ipscape/cumulus@0.0.86) (2020-01-22)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.85](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.85-develop.6...@ipscape/cumulus@0.0.85) (2020-01-16)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.84](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.84-develop.1...@ipscape/cumulus@0.0.84) (2020-01-06)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.83](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.83-develop.0...@ipscape/cumulus@0.0.83) (2019-12-13)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.82](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.82-develop.1...@ipscape/cumulus@0.0.82) (2019-12-13)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.81](http://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.81-develop.1...@ipscape/cumulus@0.0.81) (2019-12-11)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.80](http://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.80-develop.2...@ipscape/cumulus@0.0.80) (2019-12-11)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.79](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.79-develop.1...@ipscape/cumulus@0.0.79) (2019-11-26)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.78](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.77...@ipscape/cumulus@0.0.78) (2019-11-26)

**Note:** Version bump only for package @ipscape/cumulus

## [0.0.77](https://bitbucket.org/ipscapeau/ipscape-component-library/compare/@ipscape/cumulus@0.0.77-develop.0...@ipscape/cumulus@0.0.77) (2019-11-26)

**Note:** Version bump only for package @ipscape/cumulus
