process.env.VUE_CLI_BABEL_TARGET_NODE = true;
process.env.VUE_CLI_BABEL_TRANSPILE_MODULES = true;

module.exports = {
  verbose: true,
  moduleFileExtensions: ['js', 'jsx', 'json', 'vue'],
  transform: {
    '^.+\\.vue$': 'vue-jest',
    '.+\\.(css|styl|less|sass|scss|svg|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub',
    '^.+\\.jsx?$': 'babel-jest',
    '^.+\\.mdx$': '@storybook/addon-docs/jest-transform-mdx',
  },
  transformIgnorePatterns: ['/node_modules/', '<rootDir>/packages/(?:.+?)/lib/'],
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
    'ipscape/app/(.*)$': '<rootDir>/app/$1',
    '@ipscape/ips-icon': '<rootDir>/packages/components/ips-icon/src/IpsIcon.vue',
    '@ipscape/ips-countdown': '<rootDir>/packages/components/ips-countdown/src/IpsCountdown.vue',
  },
  modulePathIgnorePatterns: ['verdaccio', 'dist/'],
  snapshotSerializers: ['jest-serializer-vue'],
  testMatch: ['**/*.spec.(js|jsx|ts|tsx)|**/__tests__/*.(js|jsx|ts|tsx)'],
  testURL: 'http://localhost/',
  watchPlugins: ['jest-watch-typeahead/filename', 'jest-watch-typeahead/testname'],
  collectCoverage: true,
  collectCoverageFrom: [
    '**/*.{js,vue}',
    '!**/*.stories.js',
    '!**/index.js',
    '!**/*.eslintrc.js',
    '!**/*.config.js',
    '!**/*.prettierrc.js',
    '!**/node_modules/**',
    '!**/docs/**',
    '!**/dist/**',
    '!**/stories/**',
    '!**/tests/**',
    '!**/coverage/**',
    '!**/storybook-static/**',
  ],
  coverageReporters: ['json', 'lcov', 'text', 'clover', 'html'],
  testEnvironment: 'jsdom',
};
